cbuffer ConstBuff0 : register(b0)
{
	struct
	{
		matrix WorldMatrix;
		matrix ViewMatrix;
		matrix ProjectionMatrix;
	} Parameters;
};

cbuffer ConstBuff0 : register(b1)
{
	float DisplacementScale;
};

Texture2D Tex0          : register(t0);
SamplerState SampleType : register(s0);

struct ConstantOutputType
{
    float edges[3] 	: SV_TessFactor;
    float inside 	: SV_InsideTessFactor;
};

struct HullOutputType
{
    float3 Position 	: POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

struct PixelInputType
{
    float4 Position 	: SV_POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

[domain("tri")]
PixelInputType main(ConstantOutputType input, float3 uvwCoord : SV_DomainLocation, const OutputPatch<HullOutputType, 3> patch)
{
    float3 vertexPosition;
    PixelInputType output;
 

	output.UV = uvwCoord.x * patch[0].UV + uvwCoord.y * patch[1].UV + uvwCoord.z * patch[2].UV;
	output.Normal = uvwCoord.x * patch[0].Normal + uvwCoord.y * patch[1].Normal + uvwCoord.z * patch[2].Normal;
	output.Normal = mul(output.Normal, (float3x3)Parameters.WorldMatrix);
	
	float Displacement = Tex0.SampleLevel(SampleType, output.UV, 0).r * DisplacementScale;
	
    // Determine the position of the new vertex.
    vertexPosition = uvwCoord.x * patch[0].Position + uvwCoord.y * patch[1].Position + uvwCoord.z * patch[2].Position;
	vertexPosition += output.Normal * Displacement;
    
    // Calculate the position of the new vertex against the world, view, and projection matrices.
    output.Position = mul(float4(vertexPosition, 1.0f), Parameters.WorldMatrix);
    output.Position = mul(output.Position, Parameters.ViewMatrix);
    output.Position = mul(output.Position, Parameters.ProjectionMatrix);

	output.Tangent = uvwCoord.x * patch[0].Tangent + uvwCoord.y * patch[1].Tangent + uvwCoord.z * patch[2].Tangent;
	output.Tangent = mul(output.Tangent, (float3x3)Parameters.WorldMatrix);
	
	output.Bitangent = uvwCoord.x * patch[0].Bitangent + uvwCoord.y * patch[1].Bitangent + uvwCoord.z * patch[2].Bitangent;
	output.Bitangent = mul(output.Bitangent, (float3x3)Parameters.WorldMatrix);
    
	return output;
}