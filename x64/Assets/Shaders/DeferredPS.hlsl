Texture2D Tex0          : register(t0);
Texture2D Tex1          : register(t1);
SamplerState SampleType : register(s0);

struct PixelInputType
{
	float4 Position 	: SV_POSITION;
	float3 Normal       : NORMAL;
	float3 Tangent      : TANGENT;
	float3 Bitangent    : BITANGENT;
	float2 UV           : TEXCOORD0;
};

struct PixelOutputType
{
	float4 Color		: SV_Target0;
	float4 Normal		: SV_Target1;
};

PixelOutputType main(PixelInputType Input) : SV_TARGET
{
	PixelOutputType Output;

	Output.Color = Tex0.Sample(SampleType, Input.UV);

	float3 Normal = 2.0f * Tex1.Sample(SampleType, Input.UV).xyz - 1.0f;
	Normal = normalize(Normal.x * Input.Tangent + Normal.y * Input.Bitangent + Normal.z * Input.Normal);
	Output.Normal = float4(Normal, 1.0f);
	
	return Output;
}