cbuffer PSConstBuff0 : register(b0)
{
	float3 LightDir;
};

Texture2D ColorTexture			: register(t0);
Texture2D NormalTexture			: register(t1);
SamplerState SampleTypePoint	: register(s0);


struct PixelInputType
{
	float4 Position		: SV_POSITION;
	float2 UV			: TEXCOORD0;
};

float4 main(PixelInputType Input) : SV_TARGET
{
	float3 LightDirection = normalize(-LightDir);

	float3 Normal = NormalTexture.Sample(SampleTypePoint, Input.UV).xyz;
	float4 Color  = ColorTexture.Sample(SampleTypePoint, Input.UV) * saturate(dot(Normal, LightDirection));

	return Color;	
}
