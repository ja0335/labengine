
float2 RotateUV(float2 UV, float AngleRad)
{
	float2 TexCoord += float2(-0.5f, -0.5f);
	
	TexCoord = mul(Output.UV, float2x2(cos(AngleRad), sin(AngleRad), -sin(AngleRad), cos(AngleRad)));
	
	return TexCoord;
}