
struct VertexInputType
{
    float3 Position     : POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

struct HullInputType
{
    float3 Position     : POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

HullInputType main(VertexInputType Input)
{
    HullInputType Output;
	
    Output.Position 	= Input.Position;
    Output.Normal 		= Input.Normal;
    Output.Tangent 		= Input.Tangent;
    Output.Bitangent 	= Input.Bitangent;
    Output.UV 			= Input.UV;
    
    return Output;
}