cbuffer VSConstBuff0 : register(b0)
{
	struct
	{
		matrix WorldMatrix;
		matrix ViewMatrix;
		matrix OrthoMatrix;
	} Parameters;
};

struct VertexInputType
{
    float4 Position     : POSITION;
    float2 UV           : TEXCOORD0;
};

struct PixelInputType
{
	float4 Position		: SV_POSITION;
	float2 UV			: TEXCOORD0;
};

PixelInputType main(VertexInputType Input)
{
    PixelInputType Output;
	
	Input.Position.w = 1.0f;
	Output.Position = mul(Input.Position, Parameters.WorldMatrix);
	Output.Position = mul(Input.Position, Parameters.ViewMatrix);
	Output.Position = mul(Input.Position, Parameters.OrthoMatrix);
    Output.UV 		= Input.UV;
    
    return Output;
}