cbuffer HullShaderBuffer : register(b0)
{
	float TessellationAmount;
};

struct HullInputType
{
    float3 Position     : POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

struct ConstantOutputType
{
    float Edges[3] : SV_TessFactor;
    float Inside : SV_InsideTessFactor;
};

struct HullOutputType
{
    float3 Position 	: POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

ConstantOutputType ColorPatchConstantFunction(InputPatch<HullInputType, 3> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;


    // Set the tessellation factors for the three edges of the triangle.
    output.Edges[0] = TessellationAmount;
    output.Edges[1] = TessellationAmount;
    output.Edges[2] = TessellationAmount;

    // Set the tessellation factor for tessallating inside the triangle.
    output.Inside = TessellationAmount;

    return output;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ColorPatchConstantFunction")]
HullOutputType main(InputPatch<HullInputType, 3> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    HullOutputType output;


    // Set the position for this control point as the output position.
    output.Position = patch[pointId].Position;
    output.Normal = patch[pointId].Normal;
    output.Tangent = patch[pointId].Tangent;
    output.Bitangent = patch[pointId].Bitangent;
    output.UV = patch[pointId].UV;

    return output;
}