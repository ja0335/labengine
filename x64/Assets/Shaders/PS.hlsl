cbuffer PixelShaderBuffer : register(b0)
{
	float3 LightDir;
};

Texture2D Tex0          : register(t0);
Texture2D Tex1          : register(t1);
SamplerState SampleType : register(s0);

struct PixelInputType
{
    float4 Position 	: SV_POSITION;
    float3 Normal       : NORMAL;
    float3 Tangent      : TANGENT;
    float3 Bitangent    : BITANGENT;
    float2 UV           : TEXCOORD0;
};

float4 main(PixelInputType Input) : SV_TARGET
{
	float4 Color = 1.0f;
    float3 LightDirection = normalize(-LightDir);
	
    float3 Normal =  2.0f * Tex1.Sample(SampleType, Input.UV).xyz - 1.0f;
	Normal = normalize(Normal.x * Input.Tangent + Normal.y * Input.Bitangent + Normal.z * Input.Normal);
	float4 Diffuse = Tex0.Sample(SampleType, Input.UV) * saturate(dot(Normal, LightDirection));	
	Color = Diffuse;

    return Color;
}
