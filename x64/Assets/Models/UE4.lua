require "Quaternion"

Model  = {
    Mesh = "Assets/Meshes/UVSphere.OBJ", 
    Transform = {
		Rotation = { X = 0.0, Y = 0.0, Z = 0.0, W = 1.0 },
        Position = { X = 0.0, Y = 0.0, Z = 0.0 }, 
		Scale = { X = 2.0, Y = 2.0, Z = 2.0 }
    },
	Material = {
		TessellationAmount = 10,
		DisplacementScale = 1.0,
		
		VertexShader = "Assets/Shaders/VS.hlsl",
		HullShader = "Assets/Shaders/HS.hlsl",
		DomainShader = "Assets/Shaders/DS.hlsl",
		PixelShader = "Assets/Shaders/DeferredPS.hlsl",
		DomainShaderTextures = {
			"Assets/Textures/Brick03_disp.tga" ,
		},
		PixelShaderTextures = {
			"Assets/Textures/Brick03_col.tga" ,
			"Assets/Textures/Brick03_nrm.tga" 
		}
	},
	
	EngineData = {
		Time = 0.0,
		DeltaTime = 0.0
	},
	
	OnInit = function(self)
		SetHSParameter(0, "TessellationAmount", self.Material.TessellationAmount)
	end,
	
	OnRender = function(self)	
				
		-- if IsKeyDown(81) then -- DOWN
			-- self.Transform.Position.Y = self.Transform.Position.Y - 100.0 * self.EngineData.DeltaTime
		-- elseif  IsKeyDown(82) then -- UP
			-- self.Transform.Position.Y = self.Transform.Position.Y + 100.0 * self.EngineData.DeltaTime
		-- end
		-- self.Transform.Rotation = QuatAxisAngle({ X = 0.0, Y = 1.0, Z = 0.0 }, self.EngineData.Time)		
		-- self.Transform.Position.X = math.sin(self.EngineData.Time) * 100.0
		-- self.Transform.Position.Y = math.cos(self.EngineData.Time) * 100.0
		
	end,
	
	
	OnKeyDown = function(self, key)
		if key == "Up" then			
			self.Material.DisplacementScale = self.Material.DisplacementScale + 0.1;
			SetDSParameter(1, "DisplacementScale", self.Material.DisplacementScale)
		elseif key == "Down" then					
			self.Material.DisplacementScale = math.max(0, self.Material.DisplacementScale - 0.1);	
			SetDSParameter(1, "DisplacementScale", self.Material.DisplacementScale)
		end
	end,
}

