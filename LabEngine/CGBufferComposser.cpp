#include "CGBufferComposser.h"
#include "CD3DGraphics.h"

#include <fstream>
#include "CInput.h"

CGBufferComposser::CGBufferComposser()
{
	InputLayout = nullptr;

	VertexShader = nullptr;
	PixelShader = nullptr;

	VSConstantBuffer = nullptr;
	PSConstantBuffer = nullptr;

	VSReflection = nullptr;
	PSReflection = nullptr;

	SamplerState = nullptr;
}


CGBufferComposser::~CGBufferComposser()
{
	Shutdown();
}

void CGBufferComposser::Shutdown()
{
	// Shutdown the vertex and pixel shaders as well as the related objects.
	ShutdownShader();
}

bool CGBufferComposser::Render(
	ID3D11DeviceContext* DeviceContext, 
	int32_t IndexCount,
	std::vector<ID3D11ShaderResourceView*>& ShaderResourceViewArray,
	const XMMATRIX& WorldMatrix,
	const XMMATRIX& ViewMatrix, 
	const XMMATRIX& OrthoMatrix)
{
	// Set the shader parameters that it will use for rendering.
	if (!SetShaderParameters(DeviceContext, ShaderResourceViewArray, WorldMatrix, ViewMatrix, OrthoMatrix))
	{
		return false;
	}

	// Now render the prepared buffers with the shader.
	RenderShader(DeviceContext, IndexCount);

	return true;
}

void CGBufferComposser::EndScene(ID3D11DeviceContext* DeviceContext)
{
	for (int32_t i = 0; i < NumShaderResources; ++i)
	{
		ID3D11ShaderResourceView* nullSRV = nullptr;
		DeviceContext->PSSetShaderResources(i, 1, &nullSRV);		
	}
}

bool CGBufferComposser::InitializeShader(ID3D11Device* Device, HWND hwnd, WCHAR* VSFilename, WCHAR* PSFilename)
{
	ID3D10Blob* ErrorMessage = nullptr;
	HRESULT result = S_OK;

	// Vertex Shader 
	result = InitializeVS(Device, &ErrorMessage, VSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, VSFilename);
		return false;
	}

	VSConstantBuffer = CreateConstantBuffer(Device, &VSConstBuff0, sizeof(SVSConstBuff0));
	if (!VSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->VSSetConstantBuffers(0, 1, &VSConstantBuffer);
	
	// Pixel Shader
	result = InitializePS(Device, &ErrorMessage, PSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, PSFilename);
		return false;
	}

	PSConstantBuffer = CreateConstantBuffer(Device, &PSConstBuff0, sizeof(SPSConstBuff0));
	if (!PSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->PSSetConstantBuffers(0, 1, &PSConstantBuffer);
	
	// Create a uv sampler state description.
	D3D11_SAMPLER_DESC SamplerDesc;
	SamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	SamplerDesc.MipLODBias = 0.0f;
	SamplerDesc.MaxAnisotropy = 1;
	SamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	SamplerDesc.BorderColor[0] = 0;
	SamplerDesc.BorderColor[1] = 0;
	SamplerDesc.BorderColor[2] = 0;
	SamplerDesc.BorderColor[3] = 0;
	SamplerDesc.MinLOD = 0;
	SamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the uv sampler state.
	result = Device->CreateSamplerState(&SamplerDesc, &SamplerState);
	if (FAILED(result))
		return false;

	return true;
}

HRESULT CGBufferComposser::InitializeVS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* VSFilename)
{
	ID3D10Blob* VSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		VSFilename,
		nullptr,
		nullptr,
		"main",
		"vs_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&VSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;

	// Create the vertex shader from the buffer.
	result = Device->CreateVertexShader(
		VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(), nullptr,
		&VertexShader);

	if (FAILED(result))
		return result;

	VSReflection = nullptr;
	result = D3DReflect(VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&VSReflection));

	if (FAILED(result))
		return result;

	// Create the vertex input layout description.
	D3D11_INPUT_ELEMENT_DESC PolygonLayout[] =
	{
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	// Get a count of the elements in the layout.
	const uint32_t NumElements = sizeof(PolygonLayout) / sizeof(PolygonLayout[0]);

	// Create the vertex input layout.
	result = Device->CreateInputLayout(
		PolygonLayout,
		NumElements,
		VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(), &InputLayout);

	if (FAILED(result))
		return result;

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	VSCompiledCode->Release();
	VSCompiledCode = nullptr;

	return result;
}

HRESULT CGBufferComposser::InitializePS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* PSFilename)
{
	ID3D10Blob* PSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		PSFilename,
		nullptr,
		nullptr,
		"main",
		"ps_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,
		0,
		&PSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;

	// Create the pixel shader from the buffer.
	result = Device->CreatePixelShader(
		PSCompiledCode->GetBufferPointer(),
		PSCompiledCode->GetBufferSize(),
		nullptr,
		&PixelShader);

	if (FAILED(result))
		return result;

	PSReflection = nullptr;
	result = D3DReflect(PSCompiledCode->GetBufferPointer(),
		PSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&PSReflection));

	if (FAILED(result))
		return result;

	PSCompiledCode->Release();
	PSCompiledCode = nullptr;

	return result;
}

ID3D11Buffer* CGBufferComposser::CreateConstantBuffer(ID3D11Device* Device, void* CBDefaults, uint32_t ByteWidth)
{
	D3D11_BUFFER_DESC BufferDesc;
	BufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	BufferDesc.ByteWidth = ByteWidth;
	BufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	BufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	BufferDesc.MiscFlags = 0;
	BufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitialData;
	InitialData.pSysMem = CBDefaults;
	InitialData.SysMemPitch = 0;
	InitialData.SysMemSlicePitch = 0;

	ID3D11Buffer* ConstantBuffer = nullptr;
	Device->CreateBuffer(&BufferDesc, &InitialData, &ConstantBuffer);

	return ConstantBuffer;
}

void CGBufferComposser::HandleShaderError(HRESULT result, ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename)
{
	// If the shader failed to compile it should have writen something to the error message.
	if (ErrorMessage)
	{
		OutputShaderErrorMessage(ErrorMessage, hwnd, ShaderFilename);
	}
	// If there was nothing in the error message then it simply could not find the file itself.
	else
	{
		MessageBoxW(hwnd, ShaderFilename, L"Missing Pixel Shader File", MB_OK);
	}
}

void CGBufferComposser::ShutdownShader()
{
	// Release the sampler state.
	if (SamplerState)
	{
		SamplerState->Release();
		SamplerState = nullptr;
	}

	// Release the constant buffers.
	if (VSConstantBuffer)
	{
		VSConstantBuffer->Release();
		VSConstantBuffer = nullptr;
	}

	if (PSConstantBuffer)
	{
		PSConstantBuffer->Release();
		PSConstantBuffer = nullptr;
	}

	// Release the layout.
	if (InputLayout)
	{
		InputLayout->Release();
		InputLayout = nullptr;
	}

	// Release the pixel shader.
	if (PixelShader)
	{
		PixelShader->Release();
		PixelShader = nullptr;
	}

	// Release the vertex shader.
	if (VertexShader)
	{
		VertexShader->Release();
		VertexShader = nullptr;
	}
}

void CGBufferComposser::OutputShaderErrorMessage(ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename)
{
	std::ofstream fout;

	// Get a pointer to the error message text buffer.
	char * CompileErrors = (char*)(ErrorMessage->GetBufferPointer());

	// Get the length of the message.
	const uint64_t BufferSize = ErrorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for (uint64_t i = 0; i < BufferSize; i++)
	{
		fout << CompileErrors[i];
	}

	// Close the file.
	fout.close();

	// Release the error message.
	ErrorMessage->Release();
	ErrorMessage = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	MessageBoxW(hwnd, L"Error compiling shader.  Check shader-error.txt for message.", ShaderFilename, MB_OK);
}

bool CGBufferComposser::SetVSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = VSReflection->GetConstantBufferByIndex(ConstBufferIdx);

	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;

	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(VSConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(VSConstantBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->VSSetConstantBuffers(ConstBufferIdx, 1, &VSConstantBuffer);

	return true;
}

bool CGBufferComposser::SetPSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = PSReflection->GetConstantBufferByIndex(ConstBufferIdx);

	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;

	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(PSConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(PSConstantBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->PSSetConstantBuffers(ConstBufferIdx, 1, &PSConstantBuffer);

	return true;
}

bool CGBufferComposser::SetShaderParameters(
	ID3D11DeviceContext* DeviceContext,
	std::vector<ID3D11ShaderResourceView*>& ShaderResourceViewArray,
	XMMATRIX WorldMatrix,
	XMMATRIX ViewMatrix, 
	XMMATRIX OrthoMatrix)
{
	VSConstBuff0.WorldMatrix = XMMatrixTranspose(WorldMatrix);
	VSConstBuff0.ViewMatrix = XMMatrixTranspose(ViewMatrix);
	VSConstBuff0.OrthoMatrix = XMMatrixTranspose(OrthoMatrix);

	SetVSParameter(0, "Parameters", &VSConstBuff0);

	if (CInput::GetSingleton()->IsMouseLeftButtonDown() && !CInput::GetSingleton()->IsKeyDown(SDL_SCANCODE_LALT))
	{
		PSConstBuff0.LightDir.X += CInput::GetSingleton()->GetMouseDelta().X * 5.0f;
		PSConstBuff0.LightDir.Z += CInput::GetSingleton()->GetMouseDelta().Y * 5.0f;

		SetPSParameter(0, "LightDir", &PSConstBuff0);
	}
	
	NumShaderResources = ShaderResourceViewArray.size();

	for (int64_t i = 0; i < NumShaderResources; ++i)
	{
		DeviceContext->PSSetShaderResources(uint32_t(i), 1, &ShaderResourceViewArray[i]);
	}

	return true;
}

void CGBufferComposser::RenderShader(ID3D11DeviceContext* DeviceContext, int32_t IndexCount)
{
	// Set the vertex input layout.
	DeviceContext->IASetInputLayout(InputLayout);

	// Set the shaders that will be used to render .
	DeviceContext->VSSetShader(VertexShader, nullptr, 0);
	DeviceContext->HSSetShader(nullptr, nullptr, 0);
	DeviceContext->DSSetShader(nullptr, nullptr, 0);
	DeviceContext->PSSetShader(PixelShader, nullptr, 0);

	// Set the sampler state in the pixel shaders.
	DeviceContext->PSSetSamplers(0, 1, &SamplerState);

	// Render the triangle.
	DeviceContext->DrawIndexed(IndexCount, 0, 0);
}
