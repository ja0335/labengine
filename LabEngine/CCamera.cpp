////////////////////////////////////////////////////////////////////////////////
// Filename: cameraclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "CCamera.h"
#include "CInput.h"

CCamera::CCamera()
{
	MoveSpeed = 1.0f;
}

CCamera::~CCamera()
{
}

void CCamera::SetPosition(float X, float Y, float Z)
{
	Transform.Position.X = X;
	Transform.Position.Y = Y;
	Transform.Position.Z = Z;
}

void CCamera::Render()
{
	const SVector2 &MouseDelta = CInput::GetSingleton()->GetMouseDelta();
	
	if (CInput::GetSingleton()->IsMouseRightButtonDown())
	{
		Transform.Rotation = SQuaternion(SVector3::Up, -MouseDelta.X) * Transform.Rotation;
		Transform.Rotation = SQuaternion(Transform.Rotation.Right(), -MouseDelta.Y) * Transform.Rotation;

		const SVector3 InputVector = CInput::GetSingleton()->GetInputVector() * MoveSpeed;
		const SVector3 Forward = Transform.Rotation.Forward() * InputVector.Z;
		const SVector3 Right = Transform.Rotation.Right() * InputVector.X;
		const SVector3 Up = SVector3::Up * InputVector.Y;

		Transform.Position += (Forward + Right + Up);
	}
	else if (CInput::GetSingleton()->IsMouseMiddleButtonDown())
	{
		const SVector2 MouseDel = SVector2::Normalize(MouseDelta);
		const SVector3 Right = Transform.Rotation.Right() * -MouseDel.X * MoveSpeed;
		const SVector3 Up = Transform.Rotation.Up() * MouseDel.Y * MoveSpeed;

		Transform.Position += Right + Up;
	}
	else if (CInput::GetSingleton()->IsMouseLeftButtonDown() && CInput::GetSingleton()->IsKeyDown(SDL_SCANCODE_LALT))
	{
		Transform.Rotation = SQuaternion(SVector3::Up, -MouseDelta.X * MoveSpeed) * Transform.Rotation;
		Transform.Rotation = SQuaternion(Transform.Rotation.Right(), -MouseDelta.Y * MoveSpeed) * Transform.Rotation;

		const SQuaternion DeltaRot = SQuaternion(SVector3::Up, -MouseDelta.X * MoveSpeed) * SQuaternion(Transform.Rotation.Right(), -MouseDelta.Y * MoveSpeed);
		Transform.Position = (DeltaRot * Transform.Position) * DeltaRot.Conjugate();
	}

	ViewMatrix = XMMatrixLookAtLH(Transform.Position.GetRaw(), (Transform.Rotation.Forward() + Transform.Position).GetRaw(), Transform.Rotation.Up().GetRaw());
}

