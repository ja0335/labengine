#include "SQuaternion.h"
#include "SVector3.h"

using namespace DirectX;

const SQuaternion SQuaternion::Identity = SQuaternion();

SQuaternion::SQuaternion() :
	X(0), Y(0), Z(0), W(1)
{
}

SQuaternion::SQuaternion(float InX, float InY, float InZ, float InW) :
	X(InX), Y(InY), Z(InZ), W(InW)
{
}

SQuaternion::SQuaternion(SVector3 RotationAxis, float Angle)
{
	const XMVECTOR Q = XMQuaternionRotationAxis(RotationAxis.GetRaw(), Angle);

	const SQuaternion Result = SQuaternion(XMVectorGetX(Q), XMVectorGetY(Q), XMVectorGetZ(Q), XMVectorGetW(Q));
	X = Result.X;
	Y = Result.Y;
	Z = Result.Z;
	W = Result.W;
}

SVector3 SQuaternion::RotateVector(const SVector3& V) const
{
	return (*this * V) * Conjugate();
}

SVector3 SQuaternion::Up() const
{
	return RotateVector(SVector3::Up);
}

SVector3 SQuaternion::Right() const
{
	return RotateVector(SVector3::Right);
}

SVector3 SQuaternion::Forward() const
{
	return RotateVector(SVector3::Forward);;
}

SQuaternion SQuaternion::Conjugate() const
{
	const XMVECTOR Q = XMQuaternionConjugate(GetRaw());

	return { XMVectorGetX(Q), XMVectorGetY(Q), XMVectorGetZ(Q), XMVectorGetW(Q) };
}

SQuaternion SQuaternion::operator*(const SVector3 & V) const
{
	const XMVECTOR Q = XMQuaternionMultiply(GetRaw(), V.GetRaw());
	
	return { XMVectorGetX(Q), XMVectorGetY(Q), XMVectorGetZ(Q), XMVectorGetW(Q) };
}

SQuaternion SQuaternion::operator*(const SQuaternion& Q) const
{
	const XMVECTOR Q_ = XMQuaternionMultiply(GetRaw(), Q.GetRaw());

	return { XMVectorGetX(Q_), XMVectorGetY(Q_), XMVectorGetZ(Q_), XMVectorGetW(Q_) };
}

SQuaternion SQuaternion::operator*=(const SQuaternion& Q)
{
	const XMVECTOR Q_ = XMQuaternionMultiply(GetRaw(), Q.GetRaw());
	FromRaw(Q_);

	return *this;
}

SQuaternion::operator SVector3() const
{
	return {X, Y, Z};
}

DirectX::XMVECTOR SQuaternion::GetRaw() const
{
	return XMVectorSet(X, Y, Z, W);
}

void SQuaternion::FromRaw(const DirectX::XMVECTOR& Raw)
{
	X = XMVectorGetX(Raw);
	Y = XMVectorGetY(Raw);
	Z = XMVectorGetZ(Raw);
	W = XMVectorGetW(Raw);
}
