#pragma once

#include <directxmath.h>

struct SVector3;

struct SQuaternion
{
public:
	SQuaternion();

	SQuaternion(float InX, float InY, float InZ, float InW);

	SQuaternion(SVector3 RotationAxis, float Angle);

	SVector3 RotateVector(const SVector3 & V) const;
	
	SVector3 Up() const;

	SVector3 Right() const;

	SVector3 Forward() const;

	SQuaternion Conjugate() const;

	SQuaternion operator * (const SVector3 & V) const;

	SQuaternion operator * (const SQuaternion & Q) const;

	SQuaternion operator *= (const SQuaternion & Q);
	
	// Begin type Casting operator
	operator SVector3() const;

public:
	DirectX::XMVECTOR GetRaw() const;

protected:
	void FromRaw(const DirectX::XMVECTOR &Raw);
	
public:
	float X;
	float Y;
	float Z;
	float W;

	static const SQuaternion Identity;
};

