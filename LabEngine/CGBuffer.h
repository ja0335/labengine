#pragma once

#include <d3d11.h>
#include <cstdint>
#include <vector>
#include <cassert>

class CGBuffer
{
public:
	CGBuffer();
	
	~CGBuffer();

	std::vector<ID3D11ShaderResourceView*>& GetShaderResourceViewArray() { return ShaderResourceViewArray; }
	ID3D11ShaderResourceView * GetShaderResourceView(uint32_t ViewIdx);

	bool Initialize(ID3D11Device* Device, int textureWidth, int textureHeight, float screenDepth, float screenNear);

	void Shutdown();

	void SetRenderTargets(ID3D11DeviceContext* DeviceContext);

	void ClearRenderTargets(ID3D11DeviceContext* DeviceContext, float Red, float Green, float Blue, float Alpha);

private:
	const uint32_t					BufferCount = 2;
	uint32_t						TextureWidth;
	uint32_t						TextureHeight;
	
	std::vector<ID3D11Texture2D*>				RenderTargetTextureArray;
	std::vector<ID3D11RenderTargetView*>		RenderTargetViewArray;
	std::vector<ID3D11ShaderResourceView*>		ShaderResourceViewArray;
	ID3D11Texture2D*							DepthStencilBuffer;
	ID3D11DepthStencilView*						DepthStencilView;
	ID3D11RasterizerState*						RasterState;
	D3D11_VIEWPORT								Viewport;
};

