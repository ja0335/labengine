#pragma once

#include <directxmath.h>
#include "SModelDescriptor.h"
#include <SDL.h>

using namespace DirectX;


class CCamera
{
public:
	CCamera();

	~CCamera();

	float GetMoveSpeed() const { return MoveSpeed; }

	void SetMoveSpeed(float NewMoveSpeed) { MoveSpeed = NewMoveSpeed; }

	void SetPosition(float X, float Y, float Z);
	
	void GetViewMatrix(XMMATRIX& OutViewMatrix) const  { OutViewMatrix = ViewMatrix; }
	
	void Render();

protected:
	XMMATRIX ViewMatrix;
	STransforms Transform;
	float MoveSpeed;
};