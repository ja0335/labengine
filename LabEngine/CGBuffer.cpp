#include "CGBuffer.h"
#include "Defines.h"

CGBuffer::CGBuffer()
{
	DepthStencilBuffer = nullptr;
	DepthStencilView = nullptr;
	RasterState = nullptr;
}

CGBuffer::~CGBuffer()
{
	Shutdown();
}

ID3D11ShaderResourceView* CGBuffer::GetShaderResourceView(uint32_t ViewIdx)
{
	assert(ViewIdx < ShaderResourceViewArray.size());
	return ShaderResourceViewArray[ViewIdx];
}

bool CGBuffer::Initialize(ID3D11Device* Device, int textureWidth, int textureHeight, float screenDepth, float screenNear)
{
	HRESULT result;
	TextureWidth = textureWidth;
	TextureHeight = textureHeight;

	D3D11_TEXTURE2D_DESC RTTextureDesc;
	ZeroMemory(&RTTextureDesc, sizeof(RTTextureDesc));

	// Setup the render target texture description.
	RTTextureDesc.Width = textureWidth;
	RTTextureDesc.Height = textureHeight;
	RTTextureDesc.MipLevels = 1;
	RTTextureDesc.ArraySize = 1;
	RTTextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	RTTextureDesc.SampleDesc.Count = 1;
	RTTextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTTextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTTextureDesc.CPUAccessFlags = 0;
	RTTextureDesc.MiscFlags = 0;

	// Create the render target textures.
	for (uint32_t i = 0; i < BufferCount; ++i)
	{
		RenderTargetTextureArray.emplace_back(nullptr);
		result = Device->CreateTexture2D(&RTTextureDesc, nullptr, &RenderTargetTextureArray[i]);

		if (FAILED(result))
			return false;
	}

	// Setup the description of the render target view.
	D3D11_RENDER_TARGET_VIEW_DESC RTViewDesc;
	RTViewDesc.Format = RTTextureDesc.Format;
	RTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTViewDesc.Texture2D.MipSlice = 0;

	// Create the render target views.
	for (uint32_t i = 0; i < BufferCount; ++i)
	{
		RenderTargetViewArray.emplace_back(nullptr);
		result = Device->CreateRenderTargetView(RenderTargetTextureArray[i], &RTViewDesc, &RenderTargetViewArray[i]);

		if (FAILED(result))
			return false;
	}

	// Setup the shader resource view description.
	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	SRVDesc.Format = RTTextureDesc.Format;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MostDetailedMip = 0;
	SRVDesc.Texture2D.MipLevels = -1;

	// Create the shader resource view for the uv.
	for (uint32_t i = 0; i < BufferCount; ++i)
	{
		ShaderResourceViewArray.emplace_back(nullptr);
		result = Device->CreateShaderResourceView(RenderTargetTextureArray[i], &SRVDesc, &ShaderResourceViewArray[i]);

		if (FAILED(result))
			return false;
	}

	// Initialize the description of the depth buffer.
	D3D11_TEXTURE2D_DESC DepthBufferDesc;
	ZeroMemory(&DepthBufferDesc, sizeof(DepthBufferDesc));

	// Set up the description of the depth buffer.
	DepthBufferDesc.Width = TextureWidth;
	DepthBufferDesc.Height = TextureHeight;
	DepthBufferDesc.MipLevels = 1;
	DepthBufferDesc.ArraySize = 1;
	DepthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthBufferDesc.SampleDesc.Count = 1;
	DepthBufferDesc.SampleDesc.Quality = 0;
	DepthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	DepthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthBufferDesc.CPUAccessFlags = 0;
	DepthBufferDesc.MiscFlags = 0;

	// Create the uv for the depth buffer using the filled out description.
	result = Device->CreateTexture2D(&DepthBufferDesc, nullptr, &DepthStencilBuffer);

	if (FAILED(result))
		return false;

	// Initialize the depth stencil view.
	D3D11_DEPTH_STENCIL_VIEW_DESC DepthStencilViewDesc;
	ZeroMemory(&DepthStencilViewDesc, sizeof(DepthStencilViewDesc));

	// Set up the depth stencil view description.
	DepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DepthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	result = Device->CreateDepthStencilView(DepthStencilBuffer, &DepthStencilViewDesc, &DepthStencilView);
	
	if (FAILED(result))
		return false;

	// Setup the raster description which will determine how and what polygons will be drawn.
	D3D11_RASTERIZER_DESC RasterDesc;
	RasterDesc.AntialiasedLineEnable = false;
	RasterDesc.CullMode = D3D11_CULL_BACK;
	RasterDesc.DepthBias = 0;
	RasterDesc.DepthBiasClamp = 0.0f;
	RasterDesc.DepthClipEnable = true;
	RasterDesc.FillMode = D3D11_FILL_SOLID /*D3D11_FILL_WIREFRAME*/;
	RasterDesc.FrontCounterClockwise = false;
	RasterDesc.MultisampleEnable = false;
	RasterDesc.ScissorEnable = false;
	RasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	result = Device->CreateRasterizerState(&RasterDesc, &RasterState);
	if (FAILED(result))
	{
		return false;
	}

	
	// Setup the viewport for rendering.
	Viewport.Width = static_cast<float>(TextureWidth);
	Viewport.Height = static_cast<float>(TextureHeight);
	Viewport.MinDepth = 0.0f;
	Viewport.MaxDepth = 1.0f;
	Viewport.TopLeftX = 0.0f;
	Viewport.TopLeftY = 0.0f;

	return true;
}

void CGBuffer::Shutdown()
{
	if (RasterState)
	{
		RasterState->Release();
		RasterState = nullptr;
	}

	if(DepthStencilView)
	{
		DepthStencilView->Release();
		DepthStencilView = nullptr;
	}

	if(DepthStencilBuffer)
	{
		DepthStencilBuffer->Release();
		DepthStencilBuffer = nullptr;
	}

	for(auto & ShaderResourceView : ShaderResourceViewArray)
	{
		ShaderResourceView->Release();
		ShaderResourceView = nullptr;		
	}
	ShaderResourceViewArray.clear();

	for (auto & RenderTargetView : RenderTargetViewArray)
	{
		RenderTargetView->Release();
		RenderTargetView = nullptr;
	}
	RenderTargetViewArray.clear();

	for (auto & RenderTargetTexture : RenderTargetTextureArray)
	{
		RenderTargetTexture->Release();
		RenderTargetTexture = nullptr;
	}
	RenderTargetTextureArray.clear();
}

void CGBuffer::SetRenderTargets(ID3D11DeviceContext* DeviceContext)
{
	DeviceContext->RSSetState(RasterState);
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	DeviceContext->OMSetRenderTargets(BufferCount, &RenderTargetViewArray[0], DepthStencilView);
	DeviceContext->RSSetViewports(1, &Viewport);
}

void CGBuffer::ClearRenderTargets(ID3D11DeviceContext* DeviceContext, float Red, float Green, float Blue, float Alpha)
{
	// Setup the color to clear the buffer to.
	float Color[]{ Red, Green, Blue, Alpha };

	// Clear the render target views.
	for (auto & RenderTargetView : RenderTargetViewArray)
	{
		DeviceContext->ClearRenderTargetView(RenderTargetView, Color);
	}

	// Clear the depth buffer.
	DeviceContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}
