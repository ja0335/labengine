#include "CViewport.h"

CViewport::CViewport()
{
}

CViewport::~CViewport()
{
	Shutdown();
}

bool CViewport::Initialize(ID3D11Device* Device, int32_t WindowWidth, int32_t WindowHeight)
{
	HRESULT result;

	// Calculate the screen coordinates of the left side of the window.
	float Left = (float)((WindowWidth / 2) * -1);

	// Calculate the screen coordinates of the right side of the window.
	float Right = Left + (float)WindowWidth;

	// Calculate the screen coordinates of the top of the window.
	float Top = (float)(WindowHeight / 2);

	// Calculate the screen coordinates of the bottom of the window.
	float Bottom = Top - (float)WindowHeight;

	// Set the number of vertices in the vertex array.
	VertexCount = 6;

	// Set the number of indices in the index array.
	IndexCount = VertexCount;

	// Create the vertex array.
	VertexType * Vertices = new VertexType[VertexCount];
	if (!Vertices)
	{
		return false;
	}

	// Create the index array.
	unsigned long * Indices = new unsigned long[IndexCount];
	if (!Indices)
	{
		return false;
	}

	// Load the vertex array with data.
	// First triangle.
	Vertices[0].position = SVector3(Left, Top, 0.5f);  // Top left.
	Vertices[0].uv = SVector2(0.0f, 0.0f);

	Vertices[1].position = SVector3(Right, Bottom, 0.5f);  // Bottom right.
	Vertices[1].uv = SVector2(1.0f, 1.0f);

	Vertices[2].position = SVector3(Left, Bottom, 0.5f);  // Bottom left.
	Vertices[2].uv = SVector2(0.0f, 1.0f);

	// Second triangle.
	Vertices[3].position = SVector3(Left, Top, 0.5f);  // Top left.
	Vertices[3].uv = SVector2(0.0f, 0.0f);

	Vertices[4].position = SVector3(Right, Top, 0.5f);  // Top right.
	Vertices[4].uv = SVector2(1.0f, 0.0f);

	Vertices[5].position = SVector3(Right, Bottom, 0.5f);  // Bottom right.
	Vertices[5].uv = SVector2(1.0f, 1.0f);

	// Load the index array with data.
	for (uint32_t i = 0; i < IndexCount; i++)
	{
		Indices[i] = i;
	}

	// Set up the description of the vertex buffer.
	D3D11_BUFFER_DESC VertexBufferDesc;
	VertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	VertexBufferDesc.ByteWidth = sizeof(VertexType) * VertexCount;
	VertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	VertexBufferDesc.CPUAccessFlags = 0;
	VertexBufferDesc.MiscFlags = 0;
	VertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	D3D11_SUBRESOURCE_DATA VertexData;
	VertexData.pSysMem = Vertices;
	VertexData.SysMemPitch = 0;
	VertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
	result = Device->CreateBuffer(&VertexBufferDesc, &VertexData, &VertexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Set up the description of the index buffer.
	D3D11_BUFFER_DESC IndexBufferDesc;
	IndexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	IndexBufferDesc.ByteWidth = sizeof(unsigned long) * IndexCount;
	IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	IndexBufferDesc.CPUAccessFlags = 0;
	IndexBufferDesc.MiscFlags = 0;
	IndexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	D3D11_SUBRESOURCE_DATA IndexData;
	IndexData.pSysMem = Indices;
	IndexData.SysMemPitch = 0;
	IndexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = Device->CreateBuffer(&IndexBufferDesc, &IndexData, &IndexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] Vertices;
	Vertices = nullptr;

	delete[] Indices;
	Indices = nullptr;

	return true;
}

void CViewport::Shutdown()
{
	// Release the index buffer.
	if (IndexBuffer)
	{
		IndexBuffer->Release();
		IndexBuffer = 0;
	}

	// Release the vertex buffer.
	if (VertexBuffer)
	{
		VertexBuffer->Release();
		VertexBuffer = 0;
	}
}

void CViewport::Render(ID3D11DeviceContext* DeviceContext)
{
	// Set vertex buffer stride and offset.
	uint32_t Stride = sizeof(VertexType);
	uint32_t Offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	DeviceContext->IASetVertexBuffers(0, 1, &VertexBuffer, &Stride, &Offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	DeviceContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}
