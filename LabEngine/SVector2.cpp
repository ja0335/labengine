#include "SVector2.h"
#include <DirectXMathVector.inl>

using namespace DirectX;

SVector2::SVector2():
	SVector2(0.0f)
{
}

SVector2::SVector2(float Value) :
	SVector2(Value, Value)
{
}

SVector2::SVector2(float InX, float InY) :
	X(InX), Y(InY)
{
}

void SVector2::Normalize()
{
	const XMVECTOR V = XMVector3Normalize(GetRaw());
	X = XMVectorGetX(V);
	Y = XMVectorGetY(V);
}

SVector2 SVector2::Normalize(const SVector2& V)
{
	const XMVECTOR XMV = XMVector3Normalize(XMVectorSet(V.X, V.Y, 0, 1));

	return{ XMVectorGetX(XMV), XMVectorGetY(XMV) };
}

SVector2 SVector2::operator-(const SVector2& V) const
{
	return{ X - V.X, Y - V.Y };
}

SVector2 SVector2::operator*(float V) const
{
	return{ X * V, Y * V };
}

DirectX::XMVECTOR SVector2::GetRaw() const
{
	return XMVectorSet(X, Y, 0, 1);
}