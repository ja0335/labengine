#pragma once
#include "CSystem.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "CMaterialShader.h"
#include "SVector3.h"

using namespace DirectX;

class CGBufferComposser
{
public:
	ALIGN(16) struct SVSConstBuff0
	{
		XMMATRIX WorldMatrix		= XMMatrixIdentity();
		XMMATRIX ViewMatrix			= XMMatrixIdentity();
		XMMATRIX OrthoMatrix	= XMMatrixIdentity();
	} VSConstBuff0;
	
	ALIGN(16) struct SPSConstBuff0
	{
		SVector3 LightDir{ 0, -1, 0 };
	} PSConstBuff0;

public:
	CGBufferComposser();

	~CGBufferComposser();

	bool InitializeShader(ID3D11Device* Device, HWND hwnd, WCHAR* VSFilename, WCHAR* PSFilename);

	void Shutdown();

	bool Render(
		ID3D11DeviceContext* DeviceContext, 
		int32_t IndexCount, 
		std::vector<ID3D11ShaderResourceView*>& ShaderResourceViewArray,
		const XMMATRIX & WorldMatrix, 
		const XMMATRIX & ViewMatrix, 
		const XMMATRIX & OrthoMatrix);

	void EndScene(ID3D11DeviceContext* DeviceContext);
	
	bool SetVSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	bool SetPSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	
private:

	HRESULT InitializeVS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* VSFilename);
	HRESULT InitializePS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* PSFilename);
	ID3D11Buffer * CreateConstantBuffer(ID3D11Device* Device, void * CBDefaults, uint32_t ByteWidth);

	void HandleShaderError(HRESULT result, ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename);

	bool SetShaderParameters(
		ID3D11DeviceContext* DeviceContext,
		std::vector<ID3D11ShaderResourceView*>& ShaderResourceViewArray,
		XMMATRIX WorldMatrix, 
		XMMATRIX ViewMatrix, 
		XMMATRIX OrthoMatrix);

	void RenderShader(ID3D11DeviceContext* DeviceContext, int32_t IndexCount);

private:
	ID3D11InputLayout		* InputLayout;
	ID3D11VertexShader		* VertexShader;
	ID3D11PixelShader		* PixelShader;
	ID3D11Buffer			* VSConstantBuffer;
	ID3D11Buffer			* PSConstantBuffer;
	ID3D11ShaderReflection	* VSReflection;
	ID3D11ShaderReflection	* PSReflection;
	ID3D11SamplerState		* SamplerState;
	int64_t					  NumShaderResources;
};

