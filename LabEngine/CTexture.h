#pragma once

#include <d3d11.h>
#include <cstdint>

class CTexture
{
public:
	CTexture();
	~CTexture();

	bool Initialize(ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, const char* FileName);
	void Shutdown();

	inline ID3D11ShaderResourceView* GetTexture() const { return TextureView; }

private:
	ID3D11Texture2D				* Texture;
	ID3D11ShaderResourceView	* TextureView;
};