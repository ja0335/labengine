#pragma once

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")


#include <d3d11.h>
#include <directxmath.h>
using namespace DirectX;

class CD3DGraphics
{
public:
	CD3DGraphics();
	~CD3DGraphics();

	bool Initialize(int32_t ScreenWidth, int32_t ScreenHeight, bool VSync, HWND hwnd, bool FullScreen, float ScreenDepth, float ScreenNear);
	void Shutdown();

	void SetRenderTarget();
	void ClearRenderTarget(float Red, float Green, float Blue, float Alpha);
	void ShowScene();

	static CD3DGraphics * GetSingleton() { return Singleton; }

	inline ID3D11Device* GetDevice() const { return Device; }
	inline ID3D11DeviceContext* GetDeviceContext() const { return DeviceContext; }
	inline ID3D11RasterizerState* GetRasterState() const { return RasterState; }
	inline void SetRasterState(ID3D11RasterizerState* NewRasterState) { RasterState = NewRasterState; }


	void TurnZBufferOn();
	void TurnZBufferOff();
	
	void GetProjectionMatrix(XMMATRIX & InProjectionMatrix);
	void GetWorldMatrix(XMMATRIX & InWorldMatrix);
	void GetOrthoMatrix(XMMATRIX & InOrthoMatrix);

	void GetVideoCardInfo(char* CardName, int32_t & Memory);

private:
	static CD3DGraphics *			Singleton;
	bool							VSyncEnabled;
	int32_t							VideoCardMemory;
	char							VideoCardDescription[128];
	IDXGISwapChain*					SwapChain;
	ID3D11Device*					Device;
	ID3D11DeviceContext*			DeviceContext;
	ID3D11RenderTargetView*			RenderTargetView;
	ID3D11Texture2D*				DepthStencilBuffer;
	ID3D11DepthStencilState*		DepthStencilState;
	ID3D11DepthStencilState*		DepthStencilStateDisabled;
	ID3D11DepthStencilView*			DepthStencilView;
	ID3D11RasterizerState*			RasterState;
	XMMATRIX						ProjectionMatrix;
	XMMATRIX						WorldMatrix;
	XMMATRIX						OrthoMatrix;
	D3D11_VIEWPORT					Viewport;
};