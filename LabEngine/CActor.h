#pragma once

#include <memory>

#include <d3d11.h>
#include <directxmath.h>
#include "SVector3.h"
#include "SVector2.h"
#include "luaadapter/LuaTable.hpp"

using namespace DirectX;

#include "CMaterialShader.h"
#include "SModelDescriptor.h"

class CActor
{
private:
	struct VertexType
	{
		SVector3 position;
		SVector3 normal;
		SVector3 tangent;
		SVector3 bitangent;
		SVector2 uv;
	};

public:
	CActor(std::string InSourceFile);
	~CActor();

	bool Initialize(HWND hwnd, ID3D11Device* Device, ID3D11DeviceContext* DeviceContext);
	void Shutdown();
	void Render(ID3D11DeviceContext* DeviceContext, const XMMATRIX & ViewMatrix, const XMMATRIX & ProjectionMatrix);
	void OnKeyDown(SDL_KeyboardEvent& KeyboardEvent);

private:
	bool LoadModel(ID3D11Device* Device);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext* DeviceContext);
	void UpdateTransformFromScript();

private:
	LuaTable				* Lua;
	ID3D11Buffer			* VertexBuffer;
	ID3D11Buffer			* IndexBuffer;
	SModelDescriptor		ModelDescriptor;
	std::string				SourceFile;
	uint32_t				IndexCount;
	std::unique_ptr<CMaterialShader> MaterialShader;
};
