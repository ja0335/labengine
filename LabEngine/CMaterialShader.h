#pragma once

#include "CSystem.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <memory>
#include <vector>
#include "Defines.h"

struct SModelDescriptor;

using namespace DirectX;
using namespace std;

class CTexture;

class CMaterialShader
{
public:

	ALIGN(16) struct SVSConstBuff0
	{
	} VSConstBuff0;

	ALIGN(16) struct SHSConstBuff0
	{
		float TessellationAmount = 1.0f;
	} HSConstBuff0;

	ALIGN(16) struct SDSConstBuff0
	{
		XMMATRIX WorldMatrix = XMMatrixIdentity();
		XMMATRIX ViewMatrix = XMMatrixIdentity();
		XMMATRIX ProjectionMatrix = XMMatrixIdentity();
	} DSConstBuff0;

	ALIGN(16) struct SDSConstBuff1
	{
		float DisplacementScale = 0.0f;
	} DSConstBuff1;

	ALIGN(16) struct SPSConstBuff0
	{
	} PSConstBuff0;


public:
	CMaterialShader();
	~CMaterialShader();

	bool Initialize(const SModelDescriptor & ModelDescriptor, ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, HWND hwnd);
	void Shutdown();
	bool Render(ID3D11DeviceContext* DeviceContext, int32_t IndexCount, const XMMATRIX & WorldMatrix, const XMMATRIX & ViewMatrix, const XMMATRIX & ProjectionMatrix);

	bool SetVSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	bool SetHSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	bool SetDSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	bool SetPSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value);
	
private:
	bool InitializeShader(ID3D11Device* Device, HWND hwnd, WCHAR* VSFilename, WCHAR* HSFilename, WCHAR* DSFilename, WCHAR* PSFilename);
	HRESULT InitializeVS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* VSFilename);
	HRESULT InitializeHS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* HSFilename);
	HRESULT InitializeDS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* DSFilename);
	HRESULT InitializePS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* PSFilename);
	ID3D11Buffer * CreateConstantBuffer(ID3D11Device* Device, void * CBDefaults, uint32_t ByteWidth);

	void HandleShaderError(HRESULT result, ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename);
	void ReleaseTextures();
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename);

	bool LoadTexture(ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, std::vector<std::unique_ptr<CTexture>> & Container, const char* FileName);
		
	bool SetShaderParameters(ID3D11DeviceContext* DeviceContext, XMMATRIX WorldMatrix, XMMATRIX ViewMatrix, XMMATRIX ProjectionMatrix);
	void RenderShader(ID3D11DeviceContext* DeviceContext, int32_t IndexCount);
	void ReleaseVectorBuffer(std::vector<ID3D11Buffer*> &VectorBuffer);

private:
	ID3D11InputLayout		* InputLayout;

	ID3D11VertexShader		* VertexShader;
	ID3D11HullShader		* HullShader;
	ID3D11DomainShader		* DomainShader;
	ID3D11PixelShader		* PixelShader;
	
	std::vector<ID3D11Buffer*> VSConstantBuffers;
	std::vector<ID3D11Buffer*> HSConstantBuffers;
	std::vector<ID3D11Buffer*> DSConstantBuffers;
	std::vector<ID3D11Buffer*> PSConstantBuffers;

	ID3D11ShaderReflection	* VSReflection;
	ID3D11ShaderReflection	* HSReflection;
	ID3D11ShaderReflection	* DSReflection;
	ID3D11ShaderReflection	* PSReflection;
	
	ID3D11SamplerState* SamplerState;
	std::vector<std::unique_ptr<CTexture>> PixelShaderTextures;
	std::vector<std::unique_ptr<CTexture>> DomainShaderTextures;
};
