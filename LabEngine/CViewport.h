#pragma once

#include <d3d11.h>
#include <directxmath.h>
#include "SVector2.h"
#include "SVector3.h"

class CViewport
{
private:
	struct VertexType
	{
		SVector3 position;
		SVector2 uv;
	};
public:
	CViewport();

	~CViewport();

	uint32_t GetIndexCount() const { return IndexCount; }

	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	// Initialize the vertex and index buffer that hold the geometry for the ortho window model.
	bool Initialize(ID3D11Device* Device, int32_t WindowWidth, int32_t WindowHeight);
	void Shutdown();
	void Render(ID3D11DeviceContext* DeviceContext);

private:
	ID3D11Buffer * VertexBuffer;
	ID3D11Buffer * IndexBuffer;
	uint32_t VertexCount;
	uint32_t IndexCount;
};

