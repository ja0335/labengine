////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "CSystem.h"
#include "CInput.h"
#include "CGraphics.h"

#include <SDL_syswm.h>
#include "luaadapter/LuaAdapter.hpp"

CSystem * CSystem::Singleton = nullptr;

CSystem::CSystem()
{
	assert(Singleton == nullptr);
	Singleton = this;
	ApplicationName = "Lab Engine";
	ScreenWidth = 1280;
	ScreenHeight = 720;
	bRender = true;
	Window = nullptr;
	HWND = nullptr;
	Time = 0.0f;
}

CSystem::~CSystem()
{
	Shutdown();
}

bool CSystem::Initialize()
{
	SVector3 ClearBufferColor;

	Lua = new LuaTable("Config/AppConfig.config");
	
	if (Lua->Open("AppConfig"))
	{
		Lua->Get("ScreenWidth", ScreenWidth);
		Lua->Get("ScreenHeight", ScreenHeight);

		bool bLuaFullScreen;
		Lua->Get("FullScreen", bLuaFullScreen);
		bFullScreen = bLuaFullScreen;
				
		if (Lua->Open("ClearBufferColor"))
		{
			Lua->Get("X", ClearBufferColor.X);
			Lua->Get("Y", ClearBufferColor.Y);
			Lua->Get("Z", ClearBufferColor.Z);

			Lua->Close();
		}
	}

	// Initialize the windows api.
	InitializeWindows();

	// Create the input object.  This object will be used to handle reading the keyboard input from the user.
	Input = std::make_unique<CInput>();
	if(!Input)
	{
		return false;
	}

	// Initialize the input object.
	Input->Initialize(ScreenWidth, ScreenHeight);

	// Create the graphics object.  This object will handle rendering all the graphics for this application.
	Graphics = std::make_unique<CGraphics>();
	if(!Graphics)
	{
		return false;
	}

	// Initialize the graphics object.
	if(!Graphics->Initialize(ScreenWidth, ScreenHeight, ClearBufferColor, HWND))
	{
		return false;
	}
		
	return true;
}


void CSystem::Shutdown()
{
	if (Lua)
	{
		Lua->Close();
		delete Lua;
		Lua = nullptr;
	}
	
	// Release the graphics object.
	if(Graphics)
	{
		Graphics.reset();
	}

	// Release the input object.
	if(Input)
	{
		Input.reset();
	}

	// Shutdown the window.
	ShutdownWindows();
}


void CSystem::Run()
{
	SDL_Event Event;

	while(bRender)
	{
		while (SDL_PollEvent(&Event))
		{
			OnEvent(&Event);
		}

		if (Event.type == SDL_QUIT)
			bRender = false;
		else
			bRender = Frame();

		DeltaTime = 1.0f / 60.0f;
		Time += DeltaTime;
	}
}


bool CSystem::Frame()
{	
	if (!Input->Update())
		return false;

	// Do the frame processing for the graphics object.
	if(!Graphics->Frame())
		return false;

	return true;
}

void CSystem::OnEvent(SDL_Event* Event)
{
	switch (Event->type)
	{
	case SDL_KEYDOWN:
		Graphics->OnKeyDown(Event->key);
		break;
	case SDL_KEYUP:
		Graphics->OnKeyUp(Event->key);
		break;
	}
}

void CSystem::InitializeWindows()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) 
		return;
	
	Window = SDL_CreateWindow(ApplicationName.c_str(),
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		ScreenWidth, ScreenHeight,
		bFullScreen ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_RESIZABLE);

	SDL_SysWMinfo SystemInfo;
	SDL_VERSION(&SystemInfo.version);
	SDL_GetWindowWMInfo(Window, &SystemInfo);
	HWND = SystemInfo.info.win.window;
}

void CSystem::ShutdownWindows()
{
	SDL_DestroyWindow(Window);
	SDL_Quit();
	Window = nullptr;
	HWND = nullptr;
}
