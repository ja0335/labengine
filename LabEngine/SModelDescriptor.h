#pragma once

#include <string>
#include <vector>
#include "SQuaternion.h"
#include "SVector3.h"

struct SMaterialDescriptor
{
	std::string VertexShader;
	std::string HullShader;
	std::string DomainShader;
	std::string PixelShader;
	std::vector<std::string> PixelShaderTextures;
	std::vector<std::string> DomainShaderTextures;
};

struct STransforms
{
	SQuaternion Rotation;
	SVector3 Position;
	SVector3 Scale;
};

struct SModelDescriptor
{
	std::string Mesh;
	STransforms Transforms;
	SMaterialDescriptor Material;
};