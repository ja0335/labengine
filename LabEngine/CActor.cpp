#include <fstream>
#include <string>

#include "CActor.h"
#include <iostream>
#include "SVector3.h"
#include "luaadapter/LuaAdapter.hpp"
#include "SQuaternion.h"
#include "luaadapter/LuaFunction.hpp"
#include "CInput.h"
#include "CD3DGraphics.h"

using namespace std;

static int IsKeyDown(lua_State *L)
{
	if (!L)
		return 0;

	LuaAdapter lua{ L };
	const Sint64 Key{ lua_tointeger(L, 1) };

	if (CInput::GetSingleton()->IsKeyDown(static_cast<SDL_Scancode>(Key)))
		lua.Push(true);
	else
		lua.Push(false);

	return 1;
}

static int SetHSParameter(lua_State *L)
{
	if (!L)
		return 0;
	lua_getglobal(L, "MaterialInstance");
	if (CMaterialShader * MaterialShader = static_cast<CMaterialShader*>(lua_touserdata(L, -1)))
	{
		uint32_t ConstBufferIdx = static_cast<uint32_t>(lua_tointeger(L, 1));
		const char* VarName = lua_tostring(L, 2);
		float Value = static_cast<float>(lua_tonumber(L, 3));

		MaterialShader->SetHSParameter(ConstBufferIdx, VarName, &Value);
	}

	return 1;
}

static int SetDSParameter(lua_State *L)
{
	if (!L)
		return 0;
	lua_getglobal(L, "MaterialInstance");
	if (CMaterialShader * MaterialShader = static_cast<CMaterialShader*>(lua_touserdata(L, -1)))
	{
		uint32_t ConstBufferIdx = static_cast<uint32_t>(lua_tointeger(L, 1));
		const char* VarName = lua_tostring(L, 2);
		float Value = static_cast<float>(lua_tonumber(L, 3));

		MaterialShader->SetDSParameter(ConstBufferIdx, VarName, &Value);
	}

	return 1;
}

void SplitString(const string &Str, const string & Delimiter, vector<string> &OutResult)
{
	char * pch = strtok(const_cast<char*>(Str.c_str()), Delimiter.c_str());

	while (pch != nullptr)
	{
		OutResult.emplace_back(pch);
		pch = strtok(nullptr, Delimiter.c_str());
	}
}

void GetVertexData(const string & LineData, int32_t &v, int32_t &n, int32_t &uv)
{
	vector<string> FaceData;
	SplitString(LineData, "/", FaceData);
	if (FaceData.size() == 3)
	{
		v = strtol(FaceData[0].c_str(), nullptr, 10) - 1;
		uv = strtol(FaceData[1].c_str(), nullptr, 10) - 1;
		n = strtol(FaceData[2].c_str(), nullptr, 10) - 1;
	}
}

CActor::CActor(std::string InSourceFile)
{
	SourceFile = InSourceFile;
	VertexBuffer = nullptr;
	IndexBuffer = nullptr;
	IndexCount = 0;
}

CActor::~CActor()
{
	Shutdown();
}


bool CActor::Initialize(HWND hwnd, ID3D11Device* Device, ID3D11DeviceContext* DeviceContext)
{
	Lua = new LuaTable(SourceFile);

	LuaFunction luaFunction{ *Lua->GetLuaAdapter() };

	//lua_pushlightuserdata(L, mypointer);
	//lua_setglobal(L, "mypointer");

	// Define the C/C++-functions that can be called from lua
	luaFunction.Push(IsKeyDown, "IsKeyDown");
	luaFunction.Push(SetHSParameter, "SetHSParameter");
	luaFunction.Push(SetDSParameter, "SetDSParameter");
	
	if (Lua->Open("Model"))
	{
		Lua->Get("Mesh", ModelDescriptor.Mesh);
		
		UpdateTransformFromScript();

		if (Lua->Open("Material"))
		{
			Lua->Get("VertexShader", ModelDescriptor.Material.VertexShader);
			Lua->Get("HullShader", ModelDescriptor.Material.HullShader);
			Lua->Get("DomainShader", ModelDescriptor.Material.DomainShader);
			Lua->Get("PixelShader", ModelDescriptor.Material.PixelShader);

			if (Lua->Open("DomainShaderTextures"))
			{
				const uint32_t Length = Lua->Length();

				for (uint32_t i = 1; i <= Length; ++i)
				{
					std::string Texture;
					Lua->Get(i, Texture);

					ModelDescriptor.Material.DomainShaderTextures.push_back(Texture);
				}

				Lua->Close();
			}

			if (Lua->Open("PixelShaderTextures"))
			{
				const uint32_t Length = Lua->Length();

				for (uint32_t i = 1; i <= Length; ++i)
				{
					std::string Texture;
					Lua->Get(i, Texture);

					ModelDescriptor.Material.PixelShaderTextures.push_back(Texture);
				}

				Lua->Close();
			}

			Lua->Close();
		}
	}
	

	// Initialize the vertex and index buffers.
	if(!LoadModel(Device))
	{
		return false;
	}

	// Create the material shader object.
	MaterialShader = std::make_unique<CMaterialShader>();
	if (!MaterialShader)
	{
		return false;
	}

	// Initialize the color shader object.
	if (!MaterialShader->Initialize(ModelDescriptor, Device, DeviceContext, hwnd))
	{
		MessageBoxW(hwnd, L"Could not initialize the material shader object.", L"Error", MB_OK);
		return false;
	}


	luaFunction.PushGlobalVar("MaterialInstance", MaterialShader.get());
	
	Lua->Call("OnInit");

	return true;
}

void CActor::Shutdown()
{
	if (Lua)
	{
		Lua->Close();
		delete Lua;
		Lua = nullptr;
	}

	ShutdownBuffers();
}

void CActor::Render(ID3D11DeviceContext* DeviceContext, 
	const XMMATRIX & ViewMatrix,
	const XMMATRIX & ProjectionMatrix)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(DeviceContext);

	// Render the actor using the material shader.

	if (Lua->Open("EngineData"))
	{
		Lua->Set("Time", static_cast<float>(CSystem::GetSingleton()->GetTime()));
		Lua->Set("DeltaTime", static_cast<float>(CSystem::GetSingleton()->GetDeltaTime()));
		Lua->Close();
	}
	
	if (Lua->Call("OnRender"))
		UpdateTransformFromScript();


	const XMMATRIX Rotation = XMMatrixRotationQuaternion(
		ModelDescriptor.Transforms.Rotation.GetRaw());
	
	const XMMATRIX Translation = XMMatrixTranslation(
		ModelDescriptor.Transforms.Position.X,
		ModelDescriptor.Transforms.Position.Y,
		ModelDescriptor.Transforms.Position.Z);
	const XMMATRIX Scale = XMMatrixScaling(
		ModelDescriptor.Transforms.Scale.X,
		ModelDescriptor.Transforms.Scale.Y,
		ModelDescriptor.Transforms.Scale.Z);
	const XMMATRIX WorldMatrix = Rotation * Translation * Scale;

	MaterialShader->Render(DeviceContext, IndexCount, WorldMatrix, ViewMatrix, ProjectionMatrix);
}

void CActor::OnKeyDown(SDL_KeyboardEvent& KeyboardEvent)
{
	const std::string KeyName = SDL_GetKeyName(KeyboardEvent.keysym.sym);
	Lua->Call("OnKeyDown", KeyName);
}

bool CActor::LoadModel(ID3D11Device* Device)
{
	cout << "Loading model..." << endl;

	vector<SVector3> v;
	vector<SVector3> n;
	vector<SVector2> uv;
	vector<VertexType> Vertices;
	vector<uint32_t> Indices;
	
	ifstream ModelFile(ModelDescriptor.Mesh);

	if (ModelFile.is_open())
	{
		string Line;
		vector<string> LineData;
		vector<string> FaceData;

		while(getline(ModelFile, Line))
		{
			SplitString(Line, " ", LineData);

			if (LineData.empty())
				continue;

			if(LineData[0] == "v" && LineData.size() == 4)
			{
				SVector3 Vertex(
					-strtof(LineData[1].c_str(), nullptr), 
					strtof(LineData[2].c_str(), nullptr), 
					strtof(LineData[3].c_str(), nullptr));

				v.emplace_back(Vertex);
			}
			else if (LineData[0] == "vn" && LineData.size() == 4)
			{
				SVector3 VertexNormal(
					-strtof(LineData[1].c_str(), nullptr),
					strtof(LineData[2].c_str(), nullptr),
					strtof(LineData[3].c_str(), nullptr));

				n.emplace_back(VertexNormal);
			}
			else if (LineData[0] == "vt" && LineData.size() == 3)
			{
				SVector2 VertexTexture(
					strtof(LineData[1].c_str(), nullptr),
					strtof(LineData[2].c_str(), nullptr));

				uv.emplace_back(VertexTexture);
			}
			else if (LineData[0] == "f" && LineData.size() == 4)
			{
				int32_t v0, n0, uv0;
				int32_t v1, n1, uv1;
				int32_t v2, n2, uv2;

				GetVertexData(LineData[1], v0, n0, uv0);
				GetVertexData(LineData[3], v1, n1, uv1);
				GetVertexData(LineData[2], v2, n2, uv2);

				const SVector3 edge1 = SVector3(v[v1].X, v[v1].Y, v[v1].Z) - SVector3(v[v0].X, v[v0].Y, v[v0].Z);
				const SVector3 edge2 = SVector3(v[v2].X, v[v2].Y, v[v2].Z) - SVector3(v[v0].X, v[v0].Y, v[v0].Z);
				const SVector3 deltaUV1 = SVector3(uv[uv1].X, uv[uv1].Y, 0.0f) - SVector3(uv[uv0].X, uv[uv0].Y, 0.0f);
				const SVector3 deltaUV2 = SVector3(uv[uv2].X, uv[uv2].Y, 0.0f) - SVector3(uv[uv0].X, uv[uv0].Y, 0.0f);
				
				const float f = 1.0f / (deltaUV1.X * deltaUV2.Y - deltaUV2.X * deltaUV1.Y);
				
				SVector3 tangent;
				tangent.X = f * (deltaUV2.Y * edge1.X - deltaUV1.Y * edge2.X);
				tangent.Y = f * (deltaUV2.Y * edge1.Y - deltaUV1.Y * edge2.Y);
				tangent.Z = f * (deltaUV2.Y * edge1.Z - deltaUV1.Y * edge2.Z);
				tangent.Normalize();

				SVector3 bitangent;
				bitangent.X = f * (-deltaUV2.X * edge1.X + deltaUV1.X * edge2.X);
				bitangent.Y = f * (-deltaUV2.X * edge1.Y + deltaUV1.X * edge2.Y);
				bitangent.Z = f * (-deltaUV2.X * edge1.Z + deltaUV1.X * edge2.Z);
				bitangent.Normalize();

				const VertexType vt_0 = { v[v0] , n[n0], tangent, bitangent, uv[uv0] };
				Vertices.emplace_back(vt_0);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);

				const VertexType vt_1 = { v[v1] , n[n1], tangent, bitangent,  uv[uv1] };
				Vertices.emplace_back(vt_1);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);

				const VertexType vt_2 = { v[v2] , n[n2], tangent, bitangent,  uv[uv2] };
				Vertices.emplace_back(vt_2);
				Indices.emplace_back(uint32_t(Vertices.size()) - 1);
			}

			LineData.clear();
		}

		ModelFile.close();
	}

	v.clear();
	n.clear();
	uv.clear();


	// Set up the description of the static vertex buffer.
	D3D11_BUFFER_DESC VertexBufferDesc;
    VertexBufferDesc.ByteWidth = sizeof(VertexType) * uint32_t(Vertices.size());
    VertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
    VertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    VertexBufferDesc.CPUAccessFlags = 0;
    VertexBufferDesc.MiscFlags = 0;
	VertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	D3D11_SUBRESOURCE_DATA VertexData;
    VertexData.pSysMem = &Vertices[0];
	VertexData.SysMemPitch = 0;
	VertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    HRESULT result = Device->CreateBuffer(&VertexBufferDesc, &VertexData, &VertexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Set the number of indices in the index array.
	IndexCount = uint32_t(Indices.size());

	// Set up the description of the static index buffer.
	D3D11_BUFFER_DESC IndexBufferDesc;
    IndexBufferDesc.ByteWidth = sizeof(uint32_t) * IndexCount;
    IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
    IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    IndexBufferDesc.CPUAccessFlags = 0;
    IndexBufferDesc.MiscFlags = 0;
	IndexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	D3D11_SUBRESOURCE_DATA IndexData;
    IndexData.pSysMem = &Indices[0];
	IndexData.SysMemPitch = 0;
	IndexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = Device->CreateBuffer(&IndexBufferDesc, &IndexData, &IndexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	Vertices.clear();
	Indices.clear();
	
	cout << "End loading model..." << endl;

	return true;
}


void CActor::ShutdownBuffers()
{
	// Release the index buffer.
	if(IndexBuffer)
	{
		IndexBuffer->Release();
		IndexBuffer = nullptr;
	}

	// Release the vertex buffer.
	if(VertexBuffer)
	{
		VertexBuffer->Release();
		VertexBuffer = nullptr;
	}
}


void CActor::RenderBuffers(ID3D11DeviceContext* DeviceContext)
{
	// Set vertex buffer stride and offset.
	uint32_t Stride = sizeof(VertexType);
	uint32_t Offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	DeviceContext->IASetVertexBuffers(0, 1, &VertexBuffer, &Stride, &Offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	DeviceContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
}

void CActor::UpdateTransformFromScript()
{
	if (Lua->Open("Transform"))
	{
		if (Lua->Open("Rotation"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Rotation.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Rotation.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Rotation.Z);
			Lua->Get("W", ModelDescriptor.Transforms.Rotation.W);

			Lua->Close();
		}
		if (Lua->Open("Position"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Position.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Position.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Position.Z);

			Lua->Close();
		}
		if (Lua->Open("Scale"))
		{
			Lua->Get("X", ModelDescriptor.Transforms.Scale.X);
			Lua->Get("Y", ModelDescriptor.Transforms.Scale.Y);
			Lua->Get("Z", ModelDescriptor.Transforms.Scale.Z);

			Lua->Close();
		}

		Lua->Close();
	}
}
