#include "CGraphics.h"


CGraphics::CGraphics()
{
}

CGraphics::~CGraphics()
{
	Shutdown();
}

bool CGraphics::Initialize(int32_t ScreenWidth, int32_t ScreenHeight, const SVector3 &InClearBufferColor, HWND hwnd)
{
	ClearBufferColor = InClearBufferColor;
	// Create the Direct3D object.
	D3DGraphics = std::make_unique<CD3DGraphics>();
	if(!D3DGraphics)
	{
		return false;
	}

	// Initialize the Direct3D object.
	if(!D3DGraphics->Initialize(ScreenWidth, ScreenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR))
	{
		MessageBoxW(hwnd, L"Could not initialize Direct3D.", L"Error", MB_OK);
		return false;
	}

	Viewport = std::make_unique<CViewport>();
	if (!Viewport || !Viewport->Initialize(D3DGraphics->GetDevice(), ScreenWidth, ScreenHeight))
	{
		return false;
	}

	// Create the camera object.
	Camera = std::make_unique<CCamera>();
	if (!Camera)
	{
		return false;
	}

	// Set the initial camera position and speed.
	Camera->SetMoveSpeed(5.0f);
	Camera->SetPosition(0.0f, 0.0f, -500.0f);

	// Create the Actor.
	std::shared_ptr<CActor> Actor(new CActor("Assets/Models/UE4.lua"));

	// Initialize the model object.
	if (!Actor->Initialize(hwnd, D3DGraphics->GetDevice(), D3DGraphics->GetDeviceContext()))
	{
		MessageBoxW(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}

	Actors.push_back(move(Actor));

	// Create the G-Buffer
	GBuffer = std::make_unique<CGBuffer>();
	if (!GBuffer)
	{
		return false;
	}

	if (!GBuffer->Initialize(D3DGraphics->GetDevice(), ScreenWidth, ScreenHeight, SCREEN_DEPTH, SCREEN_NEAR))
	{
		MessageBoxW(hwnd, L"Could not initialize the G-Buffer object.", L"Error", MB_OK);
		return false;		
	}

	// Create the G-Buffer composer
	GBufferComposser = std::make_unique<CGBufferComposser>();
	if (!GBufferComposser)
	{
		return false;
	}

	if(!GBufferComposser->InitializeShader(D3DGraphics->GetDevice(), hwnd, L"Assets/Shaders/LightVS.hlsl", L"Assets/Shaders/LightPS.hlsl"))
	{
		MessageBoxW(hwnd, L"Could not initialize the G-Buffer composser object.", L"Error", MB_OK);
		return false;
	}

	return true;
}

void CGraphics::Shutdown()
{
	Actors.clear();
}

bool CGraphics::Frame()
{
	return Render();
}

void CGraphics::OnKeyDown(SDL_KeyboardEvent& KeyboardEvent)
{
	for (auto &Actor : Actors)
	{
		Actor->OnKeyDown(KeyboardEvent);
	}
}

void CGraphics::OnKeyUp(SDL_KeyboardEvent& KeyboardEvent)
{
}

bool CGraphics::Render()
{	
	if (!RenderSceneToTexture())
		return false;
	
	XMMATRIX WorldMatrix, ViewMatrix, OrthoMatrix;

	// Reset the render target back to the original back buffer.
	D3DGraphics->SetRenderTarget();

	// Clear the buffers to begin the scene.
	D3DGraphics->ClearRenderTarget(ClearBufferColor.X, ClearBufferColor.Y, ClearBufferColor.Z, 1.0f);
	
	// Generate the view matrix based on the camera's position.
	Camera->Render();

	// Get the world, view, and projection matrices from the camera and d3d objects.
	D3DGraphics->GetWorldMatrix(WorldMatrix);
	Camera->GetViewMatrix(ViewMatrix);
	D3DGraphics->GetOrthoMatrix(OrthoMatrix);

	// Turn off the Z buffer to begin all 2D rendering.
	D3DGraphics->TurnZBufferOff();

	Viewport->Render(D3DGraphics->GetDeviceContext());

	GBufferComposser->Render(
		D3DGraphics->GetDeviceContext(),
		Viewport->GetIndexCount(),
		GBuffer->GetShaderResourceViewArray(),
		WorldMatrix, ViewMatrix, OrthoMatrix);

	D3DGraphics->TurnZBufferOn();

	// Present the rendered scene to the screen.
	D3DGraphics->ShowScene();

	GBufferComposser->EndScene(D3DGraphics->GetDeviceContext());

	return true;
}

bool CGraphics::RenderSceneToTexture()
{
	XMMATRIX WorldMatrix, ViewMatrix, ProjectionMatrix;

	// Set the render buffers to be the render target.
	GBuffer->SetRenderTargets(D3DGraphics->GetDeviceContext());

	// Clear the render buffers.
	GBuffer->ClearRenderTargets(D3DGraphics->GetDeviceContext(), 0.0f, 0.0f, 0.0f, 1.0f);
	
	// Get the world, view, and projection matrices from the camera and d3d objects.
	D3DGraphics->GetWorldMatrix(WorldMatrix);
	Camera->GetViewMatrix(ViewMatrix);
	D3DGraphics->GetProjectionMatrix(ProjectionMatrix);

	// Render Actors.
	for (auto &Actor : Actors)
	{
		Actor->Render(D3DGraphics->GetDeviceContext(), ViewMatrix, ProjectionMatrix);
	}
	
	return true;
}
