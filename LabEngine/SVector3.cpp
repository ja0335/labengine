#include "SVector3.h"
#include "SVector2.h"
#include "SQuaternion.h"

using namespace DirectX;

const SVector3 SVector3::Zero = SVector3(0.0f);
const SVector3 SVector3::Up = SVector3(0.0f, 1.0f, 0.0f);
const SVector3 SVector3::Right = SVector3(1.0f, 0.0f, 0.0f);
const SVector3 SVector3::Forward = SVector3(0.0f, 0.0f, 1.0f);

SVector3::SVector3() :
	SVector3(0.0f)
{
}

SVector3::SVector3(float Value) :
	SVector3(Value, Value, Value)
{
}

SVector3::SVector3(float InX, float InY, float InZ) :
	X(InX), Y(InY), Z(InZ)
{
}

void SVector3::Normalize()
{
	const XMVECTOR V = XMVector3Normalize(GetRaw());
	X = XMVectorGetX(V);
	Y = XMVectorGetY(V);
	Z = XMVectorGetZ(V);
}

SVector3 SVector3::operator*(float S) const
{
	const XMVECTOR V = XMVectorScale(GetRaw(), S);

	return{ XMVectorGetX(V), XMVectorGetY(V), XMVectorGetZ(V) };
}

SVector3 SVector3::operator*(const SQuaternion& Q) const
{
	const XMVECTOR V = XMQuaternionMultiply(GetRaw(), Q.GetRaw());

	return { XMVectorGetX(V), XMVectorGetY(V), XMVectorGetZ(V) };
}

SVector3 SVector3::operator+(const SVector3& V) const
{
	const XMVECTOR V_ = XMVectorAdd(GetRaw(), V.GetRaw());

	return { XMVectorGetX(V_), XMVectorGetY(V_), XMVectorGetZ(V_) };
}

SVector3 SVector3::operator-(const SVector3& V) const
{
	const XMVECTOR V_ = XMVectorSubtract(GetRaw(), V.GetRaw());

	return{ XMVectorGetX(V_), XMVectorGetY(V_), XMVectorGetZ(V_) };
}

SVector3 SVector3::operator+=(const SVector3& V)
{
	*this = *this + V;

	return *this;
}

SVector3 SVector3::operator+=(const SVector2& V)
{
	*this += {V.X, V.Y, 0.0f};

	return *this;
}

XMVECTOR SVector3::GetRaw() const
{
	return XMVectorSet(X, Y, Z, 1);
}
