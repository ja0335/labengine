////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "CSystem.h"

#include <memory>

int32_t WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int32_t iCmdshow)
{
	AllocConsole();
	freopen("CONOUT$", "w+", stdout);
	freopen("CONOUT$", "w+", stderr);
	// Create the system object.
	std::unique_ptr<CSystem> System = std::make_unique<CSystem>();

	if(!System)
	{
		return 0;
	}

	// Initialize and run the system object.
	if(System->Initialize())
	{
		System->Run();
	}

	// Shutdown and release the system object.
	System.reset();

	return EXIT_SUCCESS;
}