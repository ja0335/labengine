#pragma once

///////////////////////////////
// PRE-PROCESSING DIRECTIVES //
///////////////////////////////
#define WIN32_LEAN_AND_MEAN

#include <SDL.h>

#include <windows.h>
#include <cstdint>
#include <memory>
#include <string>
#include "luaadapter/LuaTable.hpp"

#define ALIGN(x) _declspec(align(x))

class CInput;
class CGraphics;

class CSystem
{
public:
	CSystem();
	~CSystem();

	static CSystem * GetSingleton() { return Singleton; }

	/*
	* The app time since start up
	*/
	double GetTime() const { return Time; }

	/*
	 * The elapsed time since last update
	 */
	double GetDeltaTime() const { return DeltaTime; }

	bool Initialize();
	void Shutdown();
	void Run();

	void OnEvent(SDL_Event* Event);

private:
	bool Frame();
	void InitializeWindows();
	void ShutdownWindows();

private:
	static CSystem * Singleton;

	LuaTable * Lua;
	std::unique_ptr<CInput> Input;
	std::unique_ptr<CGraphics> Graphics;
	SDL_Window * Window;

	/*
	 * The app time since start up
	 */
	double Time;

	/*
	* The elapsed time since last update
	*/
	double DeltaTime;

	HWND HWND;
	int32_t ScreenWidth;
	int32_t ScreenHeight;
	uint8_t bRender : 1;
	uint8_t bFullScreen : 1;
	std::string ApplicationName;
};
