#pragma once

#include <vector>
#include <memory>

#include "CD3DGraphics.h"
#include "CCamera.h"
#include "CActor.h"
#include "CGBuffer.h"
#include "CGBufferComposser.h"
#include "CViewport.h"

const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const float SCREEN_DEPTH = 100000.0f;
const float SCREEN_NEAR = 0.1f;


class CGraphics
{
public:
	CGraphics();
	~CGraphics();

	bool Initialize(int32_t ScreenWidth, int32_t ScreenHeight, const SVector3 &InClearBufferColor, HWND hwnd);
	void Shutdown();
	bool Frame();
	void OnKeyDown(SDL_KeyboardEvent &KeyboardEvent);
	void OnKeyUp(SDL_KeyboardEvent &KeyboardEvent);

private:
	bool Render();
	bool RenderSceneToTexture();

private:
	std::unique_ptr<CD3DGraphics>		D3DGraphics;
	std::unique_ptr<CViewport>			Viewport;
	std::unique_ptr<CCamera>			Camera;
	std::unique_ptr<CGBuffer>			GBuffer;
	std::unique_ptr<CGBufferComposser>	GBufferComposser;

	std::vector<std::shared_ptr<CActor>> Actors;
	SVector3 ClearBufferColor;
};
