#pragma once

#include <directxmath.h>

struct SVector2
{
public:

	SVector2();

	SVector2(float Value);

	SVector2(float InX, float InY);

	void Normalize();

	static SVector2 Normalize(const SVector2 &V);

	SVector2 operator - (const SVector2 & V) const;

	SVector2 operator * (float V) const;

public:
	DirectX::XMVECTOR GetRaw() const;

public:
	float X;
	float Y;
};