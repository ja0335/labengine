#include "CD3DGraphics.h"

#include <dxgiformat.h>

CD3DGraphics * CD3DGraphics::Singleton = nullptr;

CD3DGraphics::CD3DGraphics()
{
	assert(Singleton == nullptr);
	Singleton = this;
	SwapChain = nullptr;
	Device = nullptr;
	DeviceContext = nullptr;
	RenderTargetView = nullptr;
	DepthStencilBuffer = nullptr;
	DepthStencilState = nullptr;
	DepthStencilStateDisabled = nullptr;
	DepthStencilView = nullptr;
	RasterState = nullptr;
}

CD3DGraphics::~CD3DGraphics()
{
	Shutdown();
}

bool CD3DGraphics::Initialize(int32_t ScreenWidth, int32_t ScreenHeight, bool VSync, HWND hwnd, bool FullScreen, float ScreenDepth, float ScreenNear)
{
	uint32_t NumModes;
	uint32_t Numerator = 0;
	uint32_t Denominator = 0;


	// Store the VSync setting.
	VSyncEnabled = VSync;

	// Create a DirectX graphics interface factory.
	IDXGIFactory* Factory;
	HRESULT result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&Factory);
	if (FAILED(result))
	{
		return false;
	}

	// Use the factory to create an adapter for the primary graphics interface (video card).
	IDXGIAdapter* Adapter;
	result = Factory->EnumAdapters(0, &Adapter);
	if (FAILED(result))
	{
		return false;
	}

	// Enumerate the primary adapter output (monitor).
	IDXGIOutput* AdapterOutput;
	result = Adapter->EnumOutputs(0, &AdapterOutput);
	if (FAILED(result))
	{
		return false;
	}

	// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
	result = AdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &NumModes, NULL);
	if (FAILED(result))
	{
		return false;
	}

	// Create a list to hold all the possible display modes for this monitor/video card combination.
	DXGI_MODE_DESC * DisplayModeList = new DXGI_MODE_DESC[NumModes];
	if (!DisplayModeList)
	{
		return false;
	}

	// Now fill the display mode list structures.
	result = AdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &NumModes, DisplayModeList);
	if (FAILED(result))
	{
		return false;
	}

	// Now go through all the display modes and find the one that matches the screen width and height.
	// When a match is found store the numerator and denominator of the refresh rate for that monitor.
	for (uint32_t i = 0; i < NumModes; i++)
	{
		if (DisplayModeList[i].Width == (uint32_t)ScreenWidth)
		{
			if (DisplayModeList[i].Height == (uint32_t)ScreenHeight)
			{
				Numerator = DisplayModeList[i].RefreshRate.Numerator;
				Denominator = DisplayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	// Get the adapter (video card) description.
	DXGI_ADAPTER_DESC AdapterDesc;
	result = Adapter->GetDesc(&AdapterDesc);
	if (FAILED(result))
	{
		return false;
	}

	// Store the dedicated video card memory in megabytes.
	VideoCardMemory = (int32_t)(AdapterDesc.DedicatedVideoMemory / 1024 / 1024);

	// Convert the name of the video card to a character array and store it.
	uint64_t StringLength;
	const int32_t error = wcstombs_s(&StringLength, VideoCardDescription, 128, AdapterDesc.Description, 128);
	if (error != 0)
	{
		return false;
	}

	// Release the display mode list.
	delete[] DisplayModeList;
	DisplayModeList = nullptr;

	// Release the adapter output.
	AdapterOutput->Release();
	AdapterOutput = nullptr;

	// Release the adapter.
	Adapter->Release();
	Adapter = nullptr;

	// Release the factory.
	Factory->Release();
	Factory = nullptr;

	// Initialize the swap chain description.
	DXGI_SWAP_CHAIN_DESC SwapChainDesc;
	ZeroMemory(&SwapChainDesc, sizeof(SwapChainDesc));

	// Set to a single back buffer.
	SwapChainDesc.BufferCount = 2;

	// Set the width and height of the back buffer.
	SwapChainDesc.BufferDesc.Width = ScreenWidth;
	SwapChainDesc.BufferDesc.Height = ScreenHeight;

	// Set regular 32-bit surface for the back buffer.
	SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	// Set the refresh rate of the back buffer.
	if (VSyncEnabled)
	{
		SwapChainDesc.BufferDesc.RefreshRate.Numerator = Numerator;
		SwapChainDesc.BufferDesc.RefreshRate.Denominator = Denominator;
	}
	else
	{
		SwapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	// Set the usage of the back buffer.
	SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	// Set the handle for the window to render to.
	SwapChainDesc.OutputWindow = hwnd;

	// Turn multisampling off.
	SwapChainDesc.SampleDesc.Count = 1;
	SwapChainDesc.SampleDesc.Quality = 0;

	// Set to full screen or windowed mode.
	if (FullScreen)
	{
		SwapChainDesc.Windowed = false;
	}
	else
	{
		SwapChainDesc.Windowed = true;
	}

	// Set the scan line ordering and scaling to unspecified.
	SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// Discard the back buffer contents after presenting.
	SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Don't set the advanced flags.
	SwapChainDesc.Flags = 0;

	// Set the feature level to DirectX 11.
	D3D_FEATURE_LEVEL FeatureLevel;
	FeatureLevel = D3D_FEATURE_LEVEL_11_0;

	// Create the swap chain, Direct3D device, and Direct3D device context.
	result = D3D11CreateDeviceAndSwapChain(
		nullptr, 
		D3D_DRIVER_TYPE_HARDWARE, 
		nullptr, 
		D3D11_CREATE_DEVICE_DEBUG, // Flag to show more info on D3D error related
		&FeatureLevel, 
		1,
		D3D11_SDK_VERSION, 
		&SwapChainDesc, 
		&SwapChain, 
		&Device, 
		nullptr, 
		&DeviceContext);

	if (FAILED(result))
	{
		return false;
	}

	// Get the pointer to the back buffer.
	ID3D11Texture2D* BackBufferPtr;
	result = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&BackBufferPtr);
	if (FAILED(result))
	{
		return false;
	}

	// Create the render target view with the back buffer pointer.
	result = Device->CreateRenderTargetView(BackBufferPtr, nullptr, &RenderTargetView);
	if (FAILED(result))
	{
		return false;
	}

	// Release pointer to the back buffer as we no longer need it.
	BackBufferPtr->Release();
	BackBufferPtr = nullptr;

	// Initialize the description of the depth buffer.
	D3D11_TEXTURE2D_DESC DepthBufferDesc;
	ZeroMemory(&DepthBufferDesc, sizeof(DepthBufferDesc));

	// Set up the description of the depth buffer.
	DepthBufferDesc.Width = ScreenWidth;
	DepthBufferDesc.Height = ScreenHeight;
	DepthBufferDesc.MipLevels = 1;
	DepthBufferDesc.ArraySize = 1;
	DepthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthBufferDesc.SampleDesc.Count = 1;
	DepthBufferDesc.SampleDesc.Quality = 0;
	DepthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	DepthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthBufferDesc.CPUAccessFlags = 0;
	DepthBufferDesc.MiscFlags = 0;

	// Create the uv for the depth buffer using the filled out description.
	result = Device->CreateTexture2D(&DepthBufferDesc, nullptr, &DepthStencilBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Initialize the description of the stencil state.
	D3D11_DEPTH_STENCIL_DESC DepthStencilDesc;
	ZeroMemory(&DepthStencilDesc, sizeof(DepthStencilDesc));

	// Set up the description of the stencil state.
	DepthStencilDesc.DepthEnable = true;
	DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	DepthStencilDesc.StencilEnable = true;
	DepthStencilDesc.StencilReadMask = 0xFF;
	DepthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	DepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	DepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	DepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	DepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	result = Device->CreateDepthStencilState(&DepthStencilDesc, &DepthStencilState);
	if (FAILED(result))
	{
		return false;
	}

	// Set the depth stencil state.
	DeviceContext->OMSetDepthStencilState(DepthStencilState, 1);
	
	// Initialize the description of the stencil state disabled.
	D3D11_DEPTH_STENCIL_DESC DepthStencilDisabledDesc;
	ZeroMemory(&DepthStencilDisabledDesc, sizeof(DepthStencilDisabledDesc));

	DepthStencilDisabledDesc.DepthEnable = false;
	DepthStencilDisabledDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DepthStencilDisabledDesc.DepthFunc = D3D11_COMPARISON_LESS;
	DepthStencilDisabledDesc.StencilEnable = true;
	DepthStencilDisabledDesc.StencilReadMask = 0xFF;
	DepthStencilDisabledDesc.StencilWriteMask = 0xFF;
	DepthStencilDisabledDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDisabledDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	DepthStencilDisabledDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDisabledDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	DepthStencilDisabledDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDisabledDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	DepthStencilDisabledDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDisabledDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	
	// Create the depth stencil state disables.
	result = Device->CreateDepthStencilState(&DepthStencilDisabledDesc, &DepthStencilStateDisabled);
	if (FAILED(result))
	{
		return false;
	}

	// Initialize the depth stencil view.
	D3D11_DEPTH_STENCIL_VIEW_DESC DepthStencilViewDesc;
	ZeroMemory(&DepthStencilViewDesc, sizeof(DepthStencilViewDesc));

	// Set up the depth stencil view description.
	DepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DepthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	result = Device->CreateDepthStencilView(DepthStencilBuffer, &DepthStencilViewDesc, &DepthStencilView);
	if (FAILED(result))
	{
		return false;
	}

	// Bind the render target view and depth stencil buffer to the output render pipeline.
	DeviceContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);

	// Setup the raster description which will determine how and what polygons will be drawn.
	D3D11_RASTERIZER_DESC RasterDesc;
	RasterDesc.AntialiasedLineEnable = false;
	RasterDesc.CullMode = D3D11_CULL_BACK;
	RasterDesc.DepthBias = 0;
	RasterDesc.DepthBiasClamp = 0.0f;
	RasterDesc.DepthClipEnable = true;
	RasterDesc.FillMode = D3D11_FILL_SOLID;
	RasterDesc.FrontCounterClockwise = false;
	RasterDesc.MultisampleEnable = false;
	RasterDesc.ScissorEnable = false;
	RasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	result = Device->CreateRasterizerState(&RasterDesc, &RasterState);
	if (FAILED(result))
	{
		return false;
	}

	// Now set the rasterizer state.
	DeviceContext->RSSetState(RasterState);

	// Setup the viewport for rendering.
	Viewport.Width = (float)ScreenWidth;
	Viewport.Height = (float)ScreenHeight;
	Viewport.MinDepth = 0.0f;
	Viewport.MaxDepth = 1.0f;
	Viewport.TopLeftX = 0.0f;
	Viewport.TopLeftY = 0.0f;

	// Create the viewport.
	DeviceContext->RSSetViewports(1, &Viewport);

	// Setup the projection matrix.
	const float FieldOfView = 3.141592654f / 4.0f;
	const float ScreenAspect = (float)ScreenWidth / (float)ScreenHeight;

	// Create the projection matrix for 3D rendering.
	ProjectionMatrix = XMMatrixPerspectiveFovLH(FieldOfView, ScreenAspect, ScreenNear, ScreenDepth);

	// Initialize the world matrix to the identity matrix.
	WorldMatrix = XMMatrixIdentity();

	// Create an orthographic projection matrix for 2D rendering.
	OrthoMatrix = XMMatrixOrthographicLH((float)ScreenWidth, (float)ScreenHeight, ScreenNear, ScreenDepth);

	return true;
}

void CD3DGraphics::Shutdown()
{
	// Before shutting down set to windowed mode or when you release the swap chain it will throw an exception.
	if (SwapChain)
	{
		SwapChain->SetFullscreenState(false, nullptr);
	}

	if (RasterState)
	{
		RasterState->Release();
		RasterState = nullptr;
	}

	if (DepthStencilView)
	{
		DepthStencilView->Release();
		DepthStencilView = nullptr;
	}

	if (DepthStencilState)
	{
		DepthStencilState->Release();
		DepthStencilState = nullptr;
	}

	if (DepthStencilBuffer)
	{
		DepthStencilBuffer->Release();
		DepthStencilBuffer = nullptr;
	}

	if (RenderTargetView)
	{
		RenderTargetView->Release();
		RenderTargetView = nullptr;
	}

	if (DeviceContext)
	{
		DeviceContext->Release();
		DeviceContext = nullptr;
	}

	if (Device)
	{
		Device->Release();
		Device = nullptr;
	}

	if (SwapChain)
	{
		SwapChain->Release();
		SwapChain = nullptr;
	}
}

void CD3DGraphics::SetRenderTarget()
{
	DeviceContext->RSSetState(RasterState);
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	DeviceContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);
	DeviceContext->RSSetViewports(1, &Viewport);
}

void CD3DGraphics::ClearRenderTarget(float Red, float Green, float Blue, float Alpha)
{
	// Setup the color to clear the buffer to.
	float Color[]{ Red, Green, Blue, Alpha};

	// Clear the back buffer.
	DeviceContext->ClearRenderTargetView(RenderTargetView, Color);

	// Clear the depth buffer.
	DeviceContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}


void CD3DGraphics::ShowScene()
{
	// Present the back buffer to the screen since rendering is complete.
	if (VSyncEnabled)
	{
		// Lock to screen refresh rate.
		SwapChain->Present(1, 0);
	}
	else
	{
		// Present as fast as possible.
		SwapChain->Present(0, 0);
	}
}

void CD3DGraphics::TurnZBufferOn()
{
	DeviceContext->OMSetDepthStencilState(DepthStencilState, 1);
}

void CD3DGraphics::TurnZBufferOff()
{
	DeviceContext->OMSetDepthStencilState(DepthStencilStateDisabled, 1);
}

void CD3DGraphics::GetProjectionMatrix(XMMATRIX & InProjectionMatrix)
{
	InProjectionMatrix = ProjectionMatrix;
}

void CD3DGraphics::GetWorldMatrix(XMMATRIX & InWorldMatrix)
{
	InWorldMatrix = WorldMatrix;
}


void CD3DGraphics::GetOrthoMatrix(XMMATRIX & InOrthoMatrix)
{
	InOrthoMatrix = OrthoMatrix;
}


void CD3DGraphics::GetVideoCardInfo(char* CardName, int32_t & Memory)
{
	strcpy_s(CardName, 128, VideoCardDescription);
	Memory = VideoCardMemory;
}