#pragma once

const float PI				= 3.141592653589793238f;
// PI / 180
const float TO_RADIANS		= 0.0174532925199432957f;

class CMath
{
public:

	static float Radians(float Degrees)
	{
		return Degrees * TO_RADIANS;
	}
};

