////////////////////////////////////////////////////////////////////////////////
// Filename: textureclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "CTexture.h"

#include <SDL_image.h>
#include <string>
#include <iostream>

CTexture::CTexture()
{
	Texture = nullptr;
	TextureView = nullptr;
}

CTexture::~CTexture()
{
	Shutdown();
}

bool CTexture::Initialize(ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, const char* FileName)
{	
	SDL_Surface * Surface = IMG_LoadTGA_RW(SDL_RWFromFile(FileName, "rb"));
	
	const std::string PixelFormat = SDL_GetPixelFormatName(Surface->format->format);
	
	// Setup the description of the uv.
	D3D11_TEXTURE2D_DESC TextureDesc;
	TextureDesc.Height = Surface->h;
	TextureDesc.Width = Surface->w;
	TextureDesc.MipLevels = 0;
	TextureDesc.ArraySize = 1;
	TextureDesc.SampleDesc.Count = 1;
	TextureDesc.SampleDesc.Quality = 0;
	TextureDesc.Usage = D3D11_USAGE_DEFAULT;
	TextureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	TextureDesc.CPUAccessFlags = 0;
	TextureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
	
	if (PixelFormat == "SDL_PIXELFORMAT_INDEX8")
		TextureDesc.Format = DXGI_FORMAT_R8_UNORM;
	else if (PixelFormat == "SDL_PIXELFORMAT_ARGB8888")
		TextureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	else
	{
		std::cerr << "No supported texture format " << PixelFormat << " for " << FileName << std::endl;
		return false;
	}

	// Create the empty uv.
	HRESULT hResult = Device->CreateTexture2D(&TextureDesc, nullptr, &Texture);
	if(FAILED(hResult))
	{
		return false;
	}
	
	// Copy the targa image data into the uv.
	DeviceContext->UpdateSubresource(Texture, 0, nullptr, Surface->pixels, Surface->pitch, 0);

	// Setup the shader resource view description.
	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	SRVDesc.Format = TextureDesc.Format;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MostDetailedMip = 0;
	SRVDesc.Texture2D.MipLevels = -1;

	// Create the shader resource view for the uv.
	hResult = Device->CreateShaderResourceView(Texture, &SRVDesc, &TextureView);
	if(FAILED(hResult))
	{
		return false;
	}

	// Generate mipmaps for this uv.
	DeviceContext->GenerateMips(TextureView);

	SDL_FreeSurface(Surface);

	return true;
}


void CTexture::Shutdown()
{
	// Release the uv view resource.
	if(TextureView)
	{
		TextureView->Release();
		TextureView = nullptr;
	}

	// Release the uv.
	if(Texture)
	{
		Texture->Release();
		Texture = nullptr;
	}
}