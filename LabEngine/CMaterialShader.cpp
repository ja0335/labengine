#include "CMaterialShader.h"
#include "CTexture.h"
#include "SModelDescriptor.h"
#include <D3DCompiler.h>
#include "CD3DGraphics.h"

#include <fstream>

CMaterialShader::CMaterialShader()
{
	InputLayout = nullptr;

	VertexShader = nullptr;
	HullShader = nullptr;
	DomainShader = nullptr;
	PixelShader = nullptr;
	
	VSReflection = nullptr;
	HSReflection = nullptr;
	DSReflection = nullptr;
	PSReflection = nullptr;

	SamplerState = nullptr;
}

CMaterialShader::~CMaterialShader()
{
	Shutdown();
}


bool CMaterialShader::Initialize(const SModelDescriptor & ModelDescriptor, ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, HWND hwnd)
{
	// Load domain shader textures.
	for (const auto & Texture : ModelDescriptor.Material.DomainShaderTextures)
	{
		if (!LoadTexture(Device, DeviceContext, DomainShaderTextures, Texture.c_str()))
			return false;
	}

	// Load pixel shader textures.
	for (const auto & Texture : ModelDescriptor.Material.PixelShaderTextures)
	{
		if (!LoadTexture(Device, DeviceContext, PixelShaderTextures, Texture.c_str()))
			return false;
	}

	// Initialize the vertex and pixel shaders.
	wstring wsVertexShader = wstring(ModelDescriptor.Material.VertexShader.begin(), ModelDescriptor.Material.VertexShader.end());
	const WCHAR * cVertexShader = wsVertexShader.c_str();
	WCHAR * VertexShader = const_cast<WCHAR*>(cVertexShader);

	wstring wsHullShader = wstring(ModelDescriptor.Material.HullShader.begin(), ModelDescriptor.Material.HullShader.end());
	const WCHAR * cHullShader = wsHullShader.c_str();
	WCHAR * HullShader = const_cast<WCHAR*>(cHullShader);

	wstring wsDomainShader = wstring(ModelDescriptor.Material.DomainShader.begin(), ModelDescriptor.Material.DomainShader.end());
	const WCHAR * cDomainShader = wsDomainShader.c_str();
	WCHAR * DomainShader = const_cast<WCHAR*>(cDomainShader);

	wstring wsPixelShader  = wstring(ModelDescriptor.Material.PixelShader.begin(), ModelDescriptor.Material.PixelShader.end());
	const WCHAR * cPixelShader = wsPixelShader.c_str();
	WCHAR * PixelShader = const_cast<WCHAR*>(cPixelShader);

	return InitializeShader(Device, hwnd, VertexShader, HullShader, DomainShader, PixelShader);
}


void CMaterialShader::Shutdown()
{
	ReleaseTextures();

	// Shutdown the vertex and pixel shaders as well as the related objects.
	ShutdownShader();
}


bool CMaterialShader::Render(ID3D11DeviceContext* DeviceContext, int32_t IndexCount, const XMMATRIX & WorldMatrix, const XMMATRIX & ViewMatrix, const XMMATRIX & ProjectionMatrix)
{
	// Set the shader parameters that it will use for rendering.
	if(!SetShaderParameters(DeviceContext, WorldMatrix, ViewMatrix, ProjectionMatrix))
	{
		return false;
	}

	// Now render the prepared buffers with the shader.
	RenderShader(DeviceContext, IndexCount);

	return true;
}


bool CMaterialShader::InitializeShader(ID3D11Device* Device, HWND hwnd, WCHAR* VSFilename, WCHAR* HSFilename, WCHAR* DSFilename, WCHAR* PSFilename)
{
	ID3D10Blob* ErrorMessage = nullptr;
	HRESULT result = S_OK;

	// Vertex Shader 
	result = InitializeVS(Device, &ErrorMessage, VSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, VSFilename);
		return false;
	}
	
	ID3D11Buffer * VSConstantBuffer = CreateConstantBuffer(Device, &VSConstBuff0, sizeof(SVSConstBuff0));
	if (!VSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->VSSetConstantBuffers(0, 1, &VSConstantBuffer);
	VSConstantBuffers.push_back(VSConstantBuffer);

	// Hull Shader
	result = InitializeHS(Device, &ErrorMessage, HSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, HSFilename);
		return false;
	}

	ID3D11Buffer * HSConstantBuffer = CreateConstantBuffer(Device, &HSConstBuff0, sizeof(SHSConstBuff0));
	if (!HSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->HSSetConstantBuffers(0, 1, &HSConstantBuffer);
	HSConstantBuffers.push_back(HSConstantBuffer);

	// Domain Shader
	result = InitializeDS(Device, &ErrorMessage, DSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, DSFilename);
		return false;
	}

	ID3D11Buffer * DSConstantBuffer;
	DSConstantBuffer = CreateConstantBuffer(Device, &DSConstBuff0, sizeof(SDSConstBuff0));
	if (!DSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->DSSetConstantBuffers(0, 1, &DSConstantBuffer);
	DSConstantBuffers.push_back(DSConstantBuffer);

	DSConstantBuffer = CreateConstantBuffer(Device, &DSConstBuff1, sizeof(SDSConstBuff1));
	if (!DSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->DSSetConstantBuffers(1, 1, &DSConstantBuffer);
	DSConstantBuffers.push_back(DSConstantBuffer);

	// Pixel Shader
	result = InitializePS(Device, &ErrorMessage, PSFilename);
	if (FAILED(result))
	{
		HandleShaderError(result, ErrorMessage, hwnd, PSFilename);
		return false;
	}

	ID3D11Buffer * PSConstantBuffer = CreateConstantBuffer(Device, &PSConstBuff0, sizeof(SPSConstBuff0));
	if (!PSConstantBuffer)
	{
		return false;
	}
	CD3DGraphics::GetSingleton()->GetDeviceContext()->PSSetConstantBuffers(0, 1, &PSConstantBuffer);
	PSConstantBuffers.push_back(PSConstantBuffer);
		
	// Create a uv sampler state description.
	D3D11_SAMPLER_DESC SamplerDesc;
	SamplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	SamplerDesc.MipLODBias = 0.0f;
	SamplerDesc.MaxAnisotropy = 16;
	SamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	SamplerDesc.BorderColor[0] = 0;
	SamplerDesc.BorderColor[1] = 0;
	SamplerDesc.BorderColor[2] = 0;
	SamplerDesc.BorderColor[3] = 0;
	SamplerDesc.MinLOD = 0;
	SamplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the uv sampler state.
	result = Device->CreateSamplerState(&SamplerDesc, &SamplerState);
	if (FAILED(result))
		return false;

	return true;
}

HRESULT CMaterialShader::InitializeVS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* VSFilename)
{
	ID3D10Blob* VSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		VSFilename,
		nullptr,
		nullptr,
		"main",
		"vs_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&VSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;

	// Create the vertex shader from the buffer.
	result = Device->CreateVertexShader(
		VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(), nullptr,
		&VertexShader);
	
	if (FAILED(result))
		return result;

	VSReflection = nullptr;
	result = D3DReflect(VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&VSReflection));

	if (FAILED(result))
		return result;

	// Create the vertex input layout description.
	// This setup needs to match the VertexType stucture in the Actor and in the shader.
	D3D11_INPUT_ELEMENT_DESC PolygonLayout[] =
	{
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BITANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	// Get a count of the elements in the layout.
	const uint32_t NumElements = sizeof(PolygonLayout) / sizeof(PolygonLayout[0]);

	// Create the vertex input layout.
	result = Device->CreateInputLayout(
		PolygonLayout,
		NumElements,
		VSCompiledCode->GetBufferPointer(),
		VSCompiledCode->GetBufferSize(), &InputLayout);
	
	if (FAILED(result))
		return result;

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	VSCompiledCode->Release();
	VSCompiledCode = nullptr;

	return result;
}

HRESULT CMaterialShader::InitializeHS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* HSFilename)
{
	ID3D10Blob* HSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		HSFilename,
		nullptr,
		nullptr,
		"main",
		"hs_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&HSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;
	
	result = Device->CreateHullShader(
		HSCompiledCode->GetBufferPointer(),
		HSCompiledCode->GetBufferSize(), nullptr,
		&HullShader);
	
	if (FAILED(result))
		return result;

	HSReflection = nullptr;
	result = D3DReflect(HSCompiledCode->GetBufferPointer(),
		HSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&HSReflection));

	if (FAILED(result))
		return result;
		
	HSCompiledCode->Release();
	HSCompiledCode = nullptr;
		
	return result;
}

HRESULT CMaterialShader::InitializeDS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* DSFilename)
{
	ID3D10Blob* DSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		DSFilename,
		nullptr,
		nullptr,
		"main",
		"ds_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS, 0,
		&DSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;

	result = Device->CreateDomainShader(
		DSCompiledCode->GetBufferPointer(),
		DSCompiledCode->GetBufferSize(), nullptr,
		&DomainShader);

	if (FAILED(result))
		return result;
	
	DSReflection = nullptr;
	result = D3DReflect(DSCompiledCode->GetBufferPointer(),
		DSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&DSReflection));

	if (FAILED(result))
		return result;

	DSCompiledCode->Release();
	DSCompiledCode = nullptr;

	return result;
}

HRESULT CMaterialShader::InitializePS(ID3D11Device* Device, ID3D10Blob** ErrorMessage, WCHAR* PSFilename)
{
	ID3D10Blob* PSCompiledCode = nullptr;

	HRESULT result = D3DCompileFromFile(
		PSFilename,
		nullptr,
		nullptr,
		"main",
		"ps_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,
		0,
		&PSCompiledCode, ErrorMessage);

	if (FAILED(result))
		return result;

	// Create the pixel shader from the buffer.
	result = Device->CreatePixelShader(
		PSCompiledCode->GetBufferPointer(),
		PSCompiledCode->GetBufferSize(),
		nullptr,
		&PixelShader);
	
	if (FAILED(result))
		return result;

	PSReflection = nullptr;
	result = D3DReflect(PSCompiledCode->GetBufferPointer(),
		PSCompiledCode->GetBufferSize(),
		IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&PSReflection));

	if (FAILED(result))
		return result;

	PSCompiledCode->Release();
	PSCompiledCode = nullptr;
	
	return result;
}

ID3D11Buffer * CMaterialShader::CreateConstantBuffer(ID3D11Device* Device, void * CBDefaults, uint32_t ByteWidth)
{
	D3D11_BUFFER_DESC BufferDesc;
	BufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	BufferDesc.ByteWidth = ByteWidth;
	BufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	BufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	BufferDesc.MiscFlags = 0;
	BufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitialData;
	InitialData.pSysMem = CBDefaults;
	InitialData.SysMemPitch = 0;
	InitialData.SysMemSlicePitch = 0;
	
	ID3D11Buffer* ConstantBuffer = nullptr;
	Device->CreateBuffer(&BufferDesc, &InitialData, &ConstantBuffer);
	
	return ConstantBuffer;
}

void CMaterialShader::HandleShaderError(HRESULT result, ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename)
{
	// If the shader failed to compile it should have writen something to the error message.
	if (ErrorMessage)
	{
		OutputShaderErrorMessage(ErrorMessage, hwnd, ShaderFilename);
	}
	// If there was nothing in the error message then it simply could not find the file itself.
	else
	{
		MessageBoxW(hwnd, ShaderFilename, L"Missing Pixel Shader File", MB_OK);
	}
}

void CMaterialShader::ReleaseTextures()
{
	for (auto & Texture : PixelShaderTextures)
	{
		Texture->Shutdown();
		Texture.release();
	}

	PixelShaderTextures.clear();

	for (auto & Texture : DomainShaderTextures)
	{
		Texture->Shutdown();
		Texture.release();
	}

	DomainShaderTextures.clear();
}

void CMaterialShader::ShutdownShader()
{
	// Release the sampler state.
	if (SamplerState)
	{
		SamplerState->Release();
		SamplerState = nullptr;
	}

	// Release the constant buffers.
	ReleaseVectorBuffer(VSConstantBuffers);
	ReleaseVectorBuffer(DSConstantBuffers);
	ReleaseVectorBuffer(HSConstantBuffers);
	ReleaseVectorBuffer(PSConstantBuffers);

	// Release the layout.
	if(InputLayout)
	{
		InputLayout->Release();
		InputLayout = nullptr;
	}

	// Release the pixel shader.
	if(PixelShader)
	{
		PixelShader->Release();
		PixelShader = nullptr;
	}

	if(DomainShader)
	{
		DomainShader->Release();
		DomainShader = nullptr;
	}

	if(HullShader)
	{
		HullShader->Release();
		HullShader = nullptr;
	}

	// Release the vertex shader.
	if(VertexShader)
	{
		VertexShader->Release();
		VertexShader = nullptr;
	}
}

void CMaterialShader::OutputShaderErrorMessage(ID3D10Blob* ErrorMessage, HWND hwnd, WCHAR* ShaderFilename)
{
	ofstream fout;

	// Get a pointer to the error message text buffer.
	char * CompileErrors = (char*)(ErrorMessage->GetBufferPointer());

	// Get the length of the message.
	const uint64_t BufferSize = ErrorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for (uint64_t i = 0; i < BufferSize; i++)
	{
		fout << CompileErrors[i];
	}

	// Close the file.
	fout.close();

	// Release the error message.
	ErrorMessage->Release();
	ErrorMessage = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	MessageBoxW(hwnd, L"Error compiling shader.  Check shader-error.txt for message.", ShaderFilename, MB_OK);
}

bool CMaterialShader::LoadTexture(ID3D11Device* Device, ID3D11DeviceContext* DeviceContext, std::vector<std::unique_ptr<CTexture>> & Container, const char* FileName)
{
	// Create the uv object.
	unique_ptr<CTexture> Texture = make_unique<CTexture>();
	if (!Texture)
	{
		return false;
	}

	// Initialize the uv object.
	if (!Texture->Initialize(Device, DeviceContext, FileName))
	{
		return false;
	}

	Container.push_back(move(Texture));

	return true;
}

bool CMaterialShader::SetVSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	assert(ConstBufferIdx < VSConstantBuffers.size());
	ID3D11Buffer * ConstBuffer = VSConstantBuffers[ConstBufferIdx];

	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = VSReflection->GetConstantBufferByIndex(ConstBufferIdx);

	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;

	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(ConstBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(ConstBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->VSSetConstantBuffers(ConstBufferIdx, 1, &ConstBuffer);

	return true;
}

bool CMaterialShader::SetHSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	assert(ConstBufferIdx < HSConstantBuffers.size());
	ID3D11Buffer * ConstBuffer = HSConstantBuffers[ConstBufferIdx];

	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = HSReflection->GetConstantBufferByIndex(ConstBufferIdx);

	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;

	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(ConstBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(ConstBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->HSSetConstantBuffers(ConstBufferIdx, 1, &ConstBuffer);

	return true;
}

bool CMaterialShader::SetDSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	assert(ConstBufferIdx < DSConstantBuffers.size());
	ID3D11Buffer * ConstBuffer = DSConstantBuffers[ConstBufferIdx];

	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = DSReflection->GetConstantBufferByIndex(ConstBufferIdx);
	
	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;
	
	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(ConstBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(ConstBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->DSSetConstantBuffers(ConstBufferIdx, 1, &ConstBuffer);

	return true;
}

bool CMaterialShader::SetPSParameter(uint32_t ConstBufferIdx, const char* VarName, void * Value)
{
	assert(ConstBufferIdx < PSConstantBuffers.size());
	ID3D11Buffer * ConstBuffer = PSConstantBuffers[ConstBufferIdx];

	ID3D11ShaderReflectionConstantBuffer * ReflectionConstBuffer = PSReflection->GetConstantBufferByIndex(ConstBufferIdx);

	if (!ReflectionConstBuffer)
		return false;

	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	ReflectionConstBuffer->GetDesc(&ShaderBufferDesc);

	ID3D11ShaderReflectionVariable * Var = ReflectionConstBuffer->GetVariableByName(VarName);

	if (!Var)
		return false;

	D3D11_SHADER_VARIABLE_DESC VarDesc;
	Var->GetDesc(&VarDesc);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	const HRESULT Result = CD3DGraphics::GetSingleton()->GetDeviceContext()->Map(ConstBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	if (FAILED(Result))
	{
		return false;
	}

	void *MappedVar = reinterpret_cast<void*>(static_cast<uint8_t*>(MappedResource.pData) + VarDesc.StartOffset);
	memcpy(MappedVar, Value, VarDesc.Size);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->Unmap(ConstBuffer, 0);

	CD3DGraphics::GetSingleton()->GetDeviceContext()->PSSetConstantBuffers(ConstBufferIdx, 1, &ConstBuffer);

	return true;
}

bool CMaterialShader::SetShaderParameters(ID3D11DeviceContext* DeviceContext, XMMATRIX WorldMatrix, XMMATRIX ViewMatrix, XMMATRIX ProjectionMatrix)
{
	// Domain Shader Buffer Parameters
	DSConstBuff0.WorldMatrix = XMMatrixTranspose(WorldMatrix);
	DSConstBuff0.ViewMatrix = XMMatrixTranspose(ViewMatrix);
	DSConstBuff0.ProjectionMatrix = XMMatrixTranspose(ProjectionMatrix);

	SetDSParameter(0, "Parameters", &DSConstBuff0);
		
	// Set shader TextureSRV resource in the domain shader.
	for (uint32_t i = 0; i < DomainShaderTextures.size(); ++i)
	{
		ID3D11ShaderResourceView* TextureSRV = DomainShaderTextures[i]->GetTexture();
		DeviceContext->DSSetShaderResources(i, 1, &TextureSRV);
	}

	// Set shader TextureSRV resource in the pixel shader.
	for (uint32_t i = 0; i < PixelShaderTextures.size(); ++i)
	{
		ID3D11ShaderResourceView* TextureSRV = PixelShaderTextures[i]->GetTexture();
		DeviceContext->PSSetShaderResources(i, 1, &TextureSRV);
	}


	return true;
}


void CMaterialShader::RenderShader(ID3D11DeviceContext* DeviceContext, int32_t IndexCount)
{
	// Set the vertex input layout.
	DeviceContext->IASetInputLayout(InputLayout);

    // Set the shaders that will be used to render this triangle.
	DeviceContext->VSSetShader(VertexShader, nullptr, 0);
	DeviceContext->HSSetShader(HullShader, nullptr, 0);
	DeviceContext->DSSetShader(DomainShader, nullptr, 0);
    DeviceContext->PSSetShader(PixelShader, nullptr, 0);

	// Set the sampler state in the domain and pixel shaders.
	DeviceContext->DSSetSamplers(0, 1, &SamplerState);
	DeviceContext->PSSetSamplers(0, 1, &SamplerState);

	// Render the triangle.
	DeviceContext->DrawIndexed(IndexCount, 0, 0);

	return;
}

void CMaterialShader::ReleaseVectorBuffer(std::vector<ID3D11Buffer*>& VectorBuffer)
{
	for (ID3D11Buffer * Buffer : VectorBuffer)
	{
		if (Buffer)
		{
			Buffer->Release();
			Buffer = nullptr;
		}
	}

	VectorBuffer.clear();
}
